var configFile = require("./config")

var AWS = require("aws-sdk");
const s3 = new AWS.S3(configFile.amazon)

let fnUpload = async() => {    
    return await s3.putObject({
        Bucket: 'cdn-vitrine/data_mining',
        Key: 'bla.json',
        Body: JSON.stringify({ token: '33f', data: 'Hello World' }),
        ContentType: 'application/json; charset=utf-8',
    }).promise();
}

fnGet = async () => {
    return await s3.getObject(
        { Bucket: "cdn-vitrine/data_mining", Key: "bla.json" }
    ).promise();
}

fnGet().then(response => {
    console.log(JSON.parse(response.Body.toString()))
}, error => {
    console.log('ERROR')
})