const path = require('path')
const nodeExternals = require('webpack-node-externals')
//const HtmlWebPackPlugin = require("html-webpack-plugin")

module.exports = {
  entry: {
    server: './src/index.js',
  },
  output: {
    filename: '[name].js'
  },
  target: 'node',
  externals: [nodeExternals()], // Need this to avoid error when working with Express
  module: {
    rules: [
      {
        // Transpiles ES6-8 into ES5
        test: /\.js$/,
        //exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  plugins: [
    /*new HtmlWebPackPlugin({
      template: "./index.html",
      filename: "./index.html",
      excludeChunks: [ 'server' ]
    })*/
  ]
}