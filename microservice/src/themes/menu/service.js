import SellMetric from "../../../../src/themes/menu/listener/metric/SellMetric"
import Conector from '../../conector'

export default {
    name: "themes.menu",
    version: 1,
    events: {
        "analytics.savedBefore": {
            async handler(ctx) {
                let params = ctx.params

                let identifier = params.identifier

                let conn = new Conector().connection(identifier)

                let sellMetric = new SellMetric(conn)
                let curMenu = await sellMetric.currentMenuCategory()
          
                if(!curMenu)
                   return false;
             
                let metricEntity = await sellMetric.getMetric(curMenu)
          
                if(metricEntity.metric == null)
                   await sellMetric.createMetric(curMenu)
                else
                   await sellMetric.addMetricTotal(metricEntity, curMenu)
            }
        }
    }
}