
import Conector from '../../../conector'
import {Config} from "core"
import {UserModel} from '../../../../../src/user/model/UserModel'
const bcrypt = require('bcrypt');

class Wrapper
{
    constructor(conn)
    {
        this.conn = conn
        this.user = null
    }

    async hash(email){
        let userModel = new UserModel(this.conn)
        let user = await userModel.findOne({email: email}, {password: 1, fullName: 1})

        if(!user)
            throw new Error(`Email não encontrado`)

        this.user = user

        const crypto = require('crypto');
        const iv = Buffer.alloc(16, 0);
        const key = crypto.scryptSync(new Buffer(user.password).toString('hex'), 'tlas', 24);

        const cipher = crypto.createCipheriv('aes-192-cbc', key, iv);

        let encrypted = cipher.update(user.password, 'utf8', 'hex');
        encrypted += cipher.final('hex')

        let hash = `${user._id}.${encrypted}`;
        return hash
    }
}

export default {
    params: {
        email: {type: "string"}
    },
    async handler(ctx) {
        let email = ctx.params.email

        let conn = new Conector().connect(Config.database.db)

        let wrapper = new Wrapper(conn)
        let hash = await wrapper.hash(email)

        this.emit2(ctx, 'v1.user.forgotPassword', {
            email: email,
            user: wrapper.user,
            recoverCode: hash
        })

        return null
    }
}