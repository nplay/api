
export default {
    params: {
        fullName: {type: "string"},
        phone: { type: "string" },
        email: { type: "string" },
        url: { type: "string" }
    },
    async handler(ctx) {

        let data = ctx.params
        this.emit2(ctx, 'v1.user.contact', data)

        return true
    }
}