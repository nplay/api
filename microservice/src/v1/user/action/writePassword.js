
import Conector from '../../../conector'
import {Config} from "core"
import {UserModel} from '../../../../../src/user/model/UserModel'
const bcrypt = require('bcrypt');

class Wrapper
{
    constructor(conn)
    {
        this.conn = conn
        this.user = null
    }

    async compareHash(code){
        let userID = null;
        let hash = null;

        let parts = code.split('.')
        userID = parts.slice(0, 1).join('')
        hash = parts.slice(1).join('')

        if(userID == '' || hash == '')
            throw new Error(`Código inválido`)

        let userModel = new UserModel(this.conn)
        this.user = await userModel.findById(userID)
    
        if(!this.user)
            throw new Error(`Email não encontrado`)

        try {
            const crypto = require('crypto');

            const algorithm = 'aes-192-cbc';
            const iv = Buffer.alloc(16, 0);
            const key = crypto.scryptSync(new Buffer(this.user.password).toString('hex'), 'tlas', 24);
            const decipher = crypto.createDecipheriv(algorithm, key, iv);
            
            let decrypted = decipher.update(hash, 'hex', 'utf8');
            decrypted += decipher.final('utf8')
    
            return decrypted == this.user.password
        } catch (error) {
            return false
        }
    }
}

export default {
    params: {
        recoverCode: {type: "string"},
        password: { type: "string" }
    },
    async handler(ctx) {
        let code = ctx.params.recoverCode

        let conn = new Conector().connect(Config.database.db)

        let wrapper = new Wrapper(conn)
        let hash = await wrapper.compareHash(code)

        if(!hash)
            throw new Error(`Código não autorizado`)

        wrapper.user.password = await bcrypt.hash(ctx.params.password, 3)
        await wrapper.user.save()

        return true
    }
}