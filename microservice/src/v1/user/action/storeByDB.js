
import Conector from '../../../conector'
import {UserModel} from '../../../../../src/user/model/UserModel'

export default {
    params: {
        identifier: {type: "string"}
    },
    async handler(ctx) {

        let conn = await new Conector().connectCore()

        let result = await UserModel(conn).findOne({
            'stores.database': ctx.params.identifier
        }, {
            'stores.$': 1
        })

        if(!result)
            return null

        return result.stores[0]
    }
}