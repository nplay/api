
import {UserModel} from '../../../../../src/user/model/UserModel'
import CreateUserHelper from '../helper/CreateUserHelper';
const bcrypt = require('bcrypt');
import Conector from '../../../conector'

class Wrapper
{
    constructor(conn)
    {
        this.conn = conn
    }

    async userExists(email)
    {
      return await UserModel(this.conn).findOne({email: email}) != null
    }

    async newUser(body)
    {
      body.password = await bcrypt.hash(body.password, 3)
      body.stores = body.stores.map(store => { store.database = new Date().getTime(); return store; })

      let objUsermodel = new UserModel(this.conn)(body);

      let createUserHelper = new CreateUserHelper(objUsermodel)

      await createUserHelper.createApplications()
      await objUsermodel.save();

      let responseData = {
        store: objUsermodel.stores[0],
        profile: objUsermodel.toObject()
      }

      delete responseData.profile.password
     return responseData
    }
}

export default {
    params: {
        fullName: {type: "string"},
        phone: {type: "string"},
        email: {type: "string"},
        password: {type: "string"},
        stores: {
             type: "array", 
             items: {
                type: "object",
                props: {
                    platform: {
                        type: 'string'
                    },
                    url: {
                        type: 'string'
                    }
                }
            }
        }
    },
    async handler(ctx) {
        
        let email = ctx.params.email
        let wrapper = new Wrapper(new Conector().connectCore())
        
        if(await wrapper.userExists(email))
            throw new Error(`Usuario já existente`)

        let data = await wrapper.newUser(ctx.params)

        this.emit2(ctx, 'v1.user.newUserAfter', data)

        return data
    }
}