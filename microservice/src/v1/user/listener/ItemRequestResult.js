import Conector from '../../../conector'
import {UserModel} from '../../../../../src/user/model/UserModel'

class ItemRequestResult
{
    async execute(content, message, ok, fail){
        try {

            var body = content.params
            var identifier = body.identifier

           if(!identifier)
                throw new Error(`Identifier not defined`)

            let conn = await new Conector().connectCore()
    
            let userModel = await UserModel(conn)

            let output =  await userModel.findOne({'stores.database': identifier}).lean()

            let storeIdx = output.stores.findIndex( store => store.database == identifier )
            
            if(storeIdx == -1)
                throw new Error(`Store not found`)

            output.stores[storeIdx].additionalData.itemLastUpdate = new Date().toISOString()
            await userModel.updateOne({'stores.database': identifier}, {
                $set: {
                    stores: output.stores
                }
            })

            ok(message)
        } catch (error) {

            this.emit2({}, 'v1.any.fatalError', {
                identifier: identifier,
                context: 'Evento "v1.user.itemRequestAfter"',
                subject: `${identifier} atualização de lastItemUpdate falhou`,
                error: error.message,
                attachments: [
                    {
                        filename: 'data.log',
                        content: JSON.stringify({identifier: identifier, params: body}),
                        contentType: 'text/plain'
                    },
                    {
                        filename: 'stackTrace.log',
                        content: error.stack,
                        contentType: 'text/plain'
                    }
                ]
            })

            fail(message)
        }
    }
}

export {
    ItemRequestResult
}