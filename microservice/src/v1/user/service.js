
import writePassword from './action/writePassword'
import forgotPassword from './action/forgotPassword'
import register from './action/register'
import contact from './action/contact'
import storeByDB from './action/storeByDB'
import { ItemRequestResult } from './listener/ItemRequestResult'

const Event2 = require("event2");

export default {
    name: "user",
    version: 1,
    mixins: [Event2],
    methods: {
    },
    actions: {
        forgotPassword,
        writePassword,
        contact,
        register,
        storeByDB
    },
    events2: [
        {
            event:'v1.item.product.itemRequestAfter',
            listeners: {
                retrive: {
                    handle: new ItemRequestResult().execute,
                    createDeadLetter: false
                }
            }
        }
    ]
}