
import PermissionHelper from '../../../../../../../src/permission/helper/permissionHelper'
import ApplicationModel from '../../../../../../../src/user/model/ApplicationModel'
import PlanEnum from '../../../../../../../src/user/model/PlanEnum'
import KeyModel from '../../../../../../../src/user/model/KeyModel'

class AstractApplication
{
    constructor(store, userModel)
    {
       this.permissionHelper = new PermissionHelper()

       this.store = store
       this.userModel = userModel
       this.alias = null

       this.scopes = {
            [PlanEnum['0'].id] : "user.path",
            [PlanEnum['1'].id] : "user.path",
            [PlanEnum['2'].id] : "user.path"
       }
    }

    get planId()
    {
        return this.store.hasOwnProperty('plan') ? this.store.plan.id : PlanEnum[0].id
    }

    get value()
    {
        this.userModel.privateKey = this.userModel.privateKey || this.permissionHelper.tokenGenerator.genPrivate()

        let publicKey = this.permissionHelper.tokenGenerator.genPublic(
            this.userModel.privateKey,
            [this.store.url],
            this.store.platform,
            this.store.database,
            this.scopes[this.planId]
        )
        .publicKey

        return publicKey
    }

    get keyModel()
    {
        let keyModel = KeyModel()

        return new keyModel({
            value: this.value
        })
    }

    get applicationModel()
    {
        let applicatonModel = ApplicationModel()

        return new applicatonModel({
            name: this.alias,
            keys: [this.keyModel]
        })
    }
}

export default AstractApplication