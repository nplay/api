
import AstractApplication from './abstractApplication'

class DashboardApplication extends AstractApplication
{
    constructor(store, userModel)
    {
        super(store, userModel)

        this.alias = "dashboard"
    }
}

export default DashboardApplication