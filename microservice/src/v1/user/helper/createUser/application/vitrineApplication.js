
import AstractApplication from './abstractApplication'

class VitrineApplication extends AstractApplication
{
    constructor(store, userModel)
    {
        super(store, userModel)

        this.alias = "vitrine"
    }
}

export default VitrineApplication