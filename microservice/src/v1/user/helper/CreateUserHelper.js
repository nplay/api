
import VitrineApplication from './createUser/application/vitrineApplication'
import DashboardApplication from './createUser/application/dashboardApplication'

class CreateUserHelper
{
    constructor(userModel)
    {
        this.userModel = userModel
    }

    async createApplications()
    {
        for(let store of this.userModel.stores)
        {
            store.applications.push(new VitrineApplication(store, this.userModel).applicationModel)
            store.applications.push(new DashboardApplication(store, this.userModel).applicationModel)
        }
    }
}

export default CreateUserHelper