
const ApiService = require("moleculer-web");
const E = require("moleculer-web").Errors;

ApiService.settings.port = 3004

let onBeforeCall = function(ctx, route, req, res) {
    // Set request headers to context meta
    ctx.meta.headers = req.headers;
    ctx.meta.req = req
}

let onAfterCall = function(ctx, route, req, res, data) {

    let responseData = {
        success: true,
        data: data
    }

    return responseData
}

let onError = function(req, res, err) {
                    
    let responseData = {
        success: false,
        data: {
            message: err.message
        }
    }

    res.setHeader("Content-Type", "application/json; charset=utf-8");
    res.end(JSON.stringify(responseData));
}

let bodyParsers = { json: true }

export default {
    mixins: [ApiService],
    settings: {
        cors: {
            // Configures the Access-Control-Allow-Origin CORS header.
            origin: "*",
            // Configures the Access-Control-Allow-Methods CORS header. 
            methods: ["GET", "OPTIONS", "POST", "PUT", "DELETE"],
            // Configures the Access-Control-Allow-Headers CORS header.
            //allowedHeaders: [],
        },
        
        routes: [
            {
                path: "/",
                authorization: true,

                onBeforeCall(ctx, route, req, res) {
                    // Set request headers to context meta
                    ctx.meta.headers = req.headers;
                }
            },

            {
                path: "/v1",
                authorization: true,
                aliases: {
                    "POST analytics/update": "v1.analytics.update",
                    "POST analytics/insert": "v1.analytics.insert",
                    "PUT analytics/buyToBuyRequest": "v1.analytics.buyToBuyRequest",

                    "GET user/status": "v1.user.status.get",
                    "PUT user/status": "v1.user.status.insert",
                    "PUT category/request": "v1.category.request",
                    "PUT category/reorder/:menuID": "v1.category.reorder",

                    "GET storage/count": "v1.storage.count",
                    "PUT storage/similarRequest": "v1.storage.similarRequest",
                    "PUT storage/itemRequest": "v1.storage.itemRequest",
                    "PUT storage/view-to-view-request": "v1.storage.viewToViewRequest",

                    "POST statistic/theme/conversion": "v1.statistic.theme.conversion",
                    "POST statistic/history/access-device": "v1.statistic.history.accessByDevice",
                    "POST statistic/history/access-geolocation": "v1.statistic.history.accessByGeolocation",
                    "POST statistic/history/item/access-hour": "v1.statistic.history.item.accessByHour",
                    "POST statistic/popup/engagement": "v1.statistic.popup.engagement",
                    "POST statistic/analytics/total/theme": "v1.statistic.analytics.total.theme",
                    "POST statistic/analytics/total/store": "v1.statistic.analytics.total.store",
                    "POST statistic/analytics/access-vs-sell": "v1.statistic.analytics.accessVsSell",
                },

                onBeforeCall,
                onAfterCall,
                onError,
                bodyParsers: bodyParsers,
            },

            {
                path: "/v1",
                authorization: false,
                aliases: {
                    "POST user/forgot-password": "v1.user.forgotPassword",
                    "POST user/write-password": "v1.user.writePassword",
                    "POST user/create": "v1.user.register",
                    "POST user/contact": "v1.user.contact"
                },

                onBeforeCall,
                onAfterCall,
                onError,
                bodyParsers: bodyParsers,
            }
        ]
    },
    methods: {
        async authorize(ctx, route, req, res) {

            // Read the token from header
            let token = req.headers["authorization-token"];

            if(!token)
                return Promise.reject(new E.UnAuthorizedError(E.ERR_NO_TOKEN));

            let response = await broker.call('v1.auth.validate', {token: token});

            if(response.body.success == false || response.body.allowed == false)
                return Promise.reject(new E.UnAuthorizedError(E.ERR_INVALID_TOKEN));
            
            let tokenPayload = JSON.parse(response.body.tokenPayload)
            req.$params.identifier = tokenPayload.dId

            return Promise.resolve(ctx);
        }
    }
}