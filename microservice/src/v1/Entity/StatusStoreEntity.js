
class StatusStoreStatusEnum
{
    constructor()
    {
        this.status = new Object({
            ocioso: 'ocioso',
            andamento: 'andamento',
            processado: 'processado',
            erro: 'erro'
        })

        this.val = null
    }

    static get OCIOSO()
    {
        return 'ocioso'
    }

    static get ANDAMENTO()
    {
        return 'andamento'
    }

    static get PROCESSADO()
    {
        return 'processado'
    }

    static get ERRO()
    {
        return 'erro'
    }

    set(val){
        if(this.status.hasOwnProperty(val) == false)
            throw new Error(`Status ${val} invalid`)

        this.val = this.status[val]
        return this
    }

    get(){
        return this.val
    }
}

class StatusStoreEntity
{
    constructor()
    {
        /** @type {Number} */
        this.code = null

        /** @type {String} */
        this.status = null

        /** @type {String} */
        this.technicalData = null

        /** @type {Date} */
        this.updated_at = null
    }

    /**
     * 
     * @param {Number} value 
     */
    async setCode(value){
        this.code = value
        return this
    }

    /**
     * 
     * @param {StatusStoreStatusEnum} statusEnum 
     */
    async setStatus(statusEnum){
        this.status = statusEnum.get()
        return this
    }

    /**
     * 
     * @param {String} value 
     */
    async setTechnicalData(value){
        this.technicalData = value
        return this
    }

    /**
     * 
     * @param {Date} date
     */
    async setUpdatedAt(date){
        this.updated_at = date
        return this
    }

}

export {
    StatusStoreEntity,
    StatusStoreStatusEnum
}