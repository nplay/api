
import Conector from '../../../conector'
import AnalyticsOrderModel from '../../../../../src/analytics/model/OrderModel'

export default {
    params: {
        identifier: { type: "string" }
    },
    async handler(ctx) {

        let identifier = ctx.params.identifier
        if(!identifier)
            throw new Error(`Identifier not found`)

        let connection = await new Conector().connect(identifier)
        let results = await AnalyticsOrderModel(connection).find({}, { "items.id": 1 })

		let recordsCollection = new Array

        for (let record of results)
        {
 			let records = new Array

            for (let item of record.items)
            {
                records.push(item.id)
                if(records.length == 4)
                    break;
            }

            recordsCollection.push(records)

            this.emit2({}, 'v1.analytics.buyToBuyRequest', {
                identifier: identifier,
                itens: recordsCollection
            })
		}
    }
}
