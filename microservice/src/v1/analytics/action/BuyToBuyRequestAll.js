
import Conector from '../../../conector'
import {UserModel} from '../../../../../src/user/model/UserModel'

export default {
    params: {
    },
    async handler(ctx) {
        let conn = new Conector().connectCore()

        let userModel = await UserModel(conn)
        let users =  await userModel.find({}, {'stores': 1, _id: 0})

        for(let user of users || [])
        {
            let identifier = null

            for(let store of user.stores || [])
            {
                try {
                    identifier = store.database
    
                    this.broker.call('v1.analytics.buyToBuyRequest', {
                        identifier: identifier
                    })
    
                } catch (error) {
                    this.emit2({}, 'v1.any.fatalError', {
                        identifier: identifier,
                        context: 'Action BuyToBuyRequestAll',
                        subject: `${identifier} solicitação falhou`,
                        error: error.message,
                        attachments: [
                            {
                                filename: 'data.log',
                                content: JSON.stringify({
                                    identifier: identifier,
                                    data: {
                                        store: store || null
                                    }
                                }),
                                contentType: 'text/plain'
                            },
                            {
                                filename: 'stackTrace.log',
                                content: error.stack,
                                contentType: 'text/plain'
                            }
                        ]
                    })

                    throw new Error(error.message)
                }
            }
        }
    }
}
