
import Conector from '../../../conector'
import BuyToBuyModel from '../../../../../src/mining/buyToBuy/model/buyToBuyModel'

class BuyToBuyResult
{
    async execute(content, message, ok, fail){

        let body = content.params

        let itens = body.itens
        let identifier = body.identifier

        try {
           if(!itens || !identifier)
                throw new Error(`Itens or identifier not defined`)

            let conn = await new Conector().connect(identifier)

            await BuyToBuyModel(conn).remove({})
            await BuyToBuyModel(conn).insertMany(itens)

            ok(message)
        } catch (error) {

            this.emit2({}, 'v1.any.fatalError', {
                identifier: identifier,
                context: 'Listener v1.data-mining.buyToBuyRequest',
                subject: `${identifier} solicitação falhou`,
                error: error.message,
                attachments: [
                    {
                        filename: 'data.log',
                        content: JSON.stringify({
                            identifier: identifier,
                            data: body
                        }),
                        contentType: 'text/plain'
                    },
                    {
                        filename: 'stackTrace.log',
                        content: error.stack,
                        contentType: 'text/plain'
                    }
                ]
            })

            fail(message)
        }
    }
}

export {
    BuyToBuyResult
}












/* 



        let connection = null
        let messages = await this.consumerMessage.read()
    
        if(messages.length == 0)
            return await this.consumerMessage.clearMessages(messages)

        let message = messages[0]
        let content = message.content
        let database = message.identifier

        connection = await Conector.connect(database)
        
        await BuyToBuyModel(connection).remove({})
        await BuyToBuyModel(connection).insertMany(content)
        
        await this.consumerMessage.clearMessages(messages)
        return true */