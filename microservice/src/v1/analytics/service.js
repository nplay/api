
import ItemModel from "../../../../src/storage/model/ItemModel";
import DateHelper from "../../../../src/helper/DateHelper";
import Conector from '../../conector'

import AnalyticsOrderModel from '../../../../src/analytics/model/OrderModel'
const Event2 = require("event2");
const Cron = require("moleculer-cron");

import BuyToBuyRequest from './action/BuyToBuyRequest';
import BuyToBuyRequestAll from './action/BuyToBuyRequestAll';
import { BuyToBuyResult } from "./listener/BuyToBuyResult";

export default {
    name: "analytics",
    version: 1,
    _identifier: null,
    mixins: [Event2, Cron],
    methods: {
        async createOrder(body){

            let conn = new Conector().connection(this._identifier)

            //Prepare data
            let itens = body.items
    
            for(let item of itens)
            {
                let storeItem = await ItemModel(conn).findOne({
                    $or: [{
                            id: item.id
                        },
                        {
                            entityId: item.entityId
                        }]
                }).lean()
    
                if(storeItem == null)
                    throw new Error(`Item not found in store '${item.id}'`)
                
                item.categorys = storeItem.categorys
            }
        
            let analyticsData = Object.assign({
                created_at: new DateHelper().toTimezone().toISOString()
            }, body)

            return await AnalyticsOrderModel(conn).create(analyticsData)
        }
    },

    actions: {

        buyToBuyRequest: BuyToBuyRequest,
        buyToBuyRequestAll: BuyToBuyRequestAll,

        async insert(ctx) {
            
            let response = {
                success: true,
                data: null
            }

            try {
                this._identifier = ctx.params.identifier
                let body = ctx.params

                let order = await this.createOrder(body)

                broker.emit("analytics.savedBefore", {
                    order: order,
                    sessionID: body.sessionID,
                    identifier: this._identifier
                });

            } catch (error) {
                response.success = false
                response.info = error.toString()
            }

            return response
        },

        async update(ctx) {
            
            let response = {
                success: true,
                data: null
            }

            try {
                this._identifier = ctx.params.identifier
                let body = ctx.params
                let conn = new Conector().connection(this._identifier)

                let order = await AnalyticsOrderModel(conn).findOne({
                    _id: body.order._id
                })

                order = Object.assign(order, body.order)
                await order.save()

                return response

            } catch (error) {
                response.success = false
                response.info = error.toString()
            }

            return response
        }
    },

    events2: [
        {
            event:'v1.data-mining.buyToBuyRequest',
            listeners: {
                retrive: {
                    handle: new BuyToBuyResult().execute,
                    createDeadLetter: false
                }
            },
        }
    ],

    crons: [
        {
            name: "BuyToBuyRequestAllSchedule",
            cronTime: '1 */6 * * *',
            onTick: function() {
                this.call('v1.analytics.buyToBuyRequestAll')
            },
            timeZone: 'America/Sao_Paulo'
        }
    ]
}