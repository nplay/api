
import request from './action/request'
import { Retrive } from './listener/get';
import reorder from './action/reorder';
import { ReorderComplete } from './listener/reorderComplete';
import { ReorderFail } from './listener/reorderFail';
const Event2 = require("event2");

export default {
    name: "category",
    version: 1,
    mixins: [Event2],
    methods: {
    },
    actions: {
        request: request,
        reorder: reorder
    },
    events2: [
        {
            event:'v1.ipapi.getCategoryResult',
            listeners: {
                result: {
                    handle: new Retrive().execute,
                    createDeadLetter: false
                }
            },
        },
        {
            event:'v1.ipapi.reorderCategoryCompleted',
            listeners: {
                result: {
                    handle: new ReorderComplete().execute,
                    createDeadLetter: false
                }
            },
        },
        {
            event:'v1.ipapi.reorderCategoryFail',
            listeners: {
                result: {
                    handle: new ReorderFail().execute,
                    createDeadLetter: false
                }
            }
        }
    ]
}