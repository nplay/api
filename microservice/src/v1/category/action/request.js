
export default {
    params: {
        identifier: {
            type: "string"
        }
    },
    async handler(ctx) {

        let identifier = ctx.params.identifier

        let store = await broker.call('v1.user.storeByDB', {
            identifier: identifier
        })

        if(!store)
            throw new Error(`Store not found with identifier: ${identifier}, impossible retrive category`)

        let body = {
            identifier: identifier,
            store: store
        }

        this.emit2(ctx, 'v1.category.get', body)
        return true
    }
}