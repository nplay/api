import MenuModel from "../../../../../src/themes/menu/model/MenuModel"
import Conector from '../../../conector'

export default {
    params: {
        identifier: {
            type: "string"
        },
        menuID: {
            type: "string"
        }
    },
    async handler(ctx) {

        let identifier = ctx.params.identifier
        let menuID = ctx.params.menuID

        let conn = new Conector().connection(identifier)
        let menuModel = await MenuModel(conn).findOne({_id: menuID})

        if(!menuModel)
            throw new Error(`Menu not found`)

        let store = await broker.call('v1.user.storeByDB', {
            identifier: identifier
        })

        if(!store)
            throw new Error(`Store not found with identifier: ${identifier}, impossible retrive category`)

        menuModel.status = 'sending'
        await menuModel.save()

        this.emit2(ctx, 'v1.category.reorder', {
            identifier: identifier,
            menu: menuModel.data,
            menuID: menuID,
            store: store
        })

        return null
    }
}