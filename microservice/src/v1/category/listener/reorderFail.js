import Conector from '../../../conector'
import MenuModel from '../../../../../src/themes/menu/model/MenuModel'

class ReorderFail
{
    async execute(content, message, ok, fail){

        let menu = null

        try {
            let body = content.params

            let identifier = body.identifier
            let menuID = body.menuID

            if(!identifier)
                throw new Error(`Identifier not defined`)
            else if (!menuID)
                throw new Error(`MenuID not defined`)

            let conn = await new Conector().connect(identifier)

            menu = await MenuModel(conn).findOne({ _id: menuID })

            if(!menu)
                throw new Error(`Menu not found with _id: ${menuID} identifier: ${identifier}`)

            menu.status = 'error'
            menu.lastCurrentWrite = new Date().toISOString()
            menu.isCurrent = true
            await menu.save()
                
            ok(message)
        } catch (error) {
            console.log(error)
            fail(message)
        }
    }
}

export {
    ReorderFail
}