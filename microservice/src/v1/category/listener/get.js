import CategoryModel from "../../../../../src/category/model/CategoryModel";

import Conector from '../../../conector'

class Retrive
{
    async execute(content, message, ok, fail){
        try {
            let body = content.params

            let categorys = body.categorys
            let identifier = body.identifier

           if(!categorys || !identifier)
                throw new Error(`Host or identifier not defined`)

            let connection = await new Conector().connect(identifier)
    
            //Delete all categorys
            await CategoryModel(connection).deleteMany();
    
            //Create all
            await CategoryModel(connection).create(categorys)
                
            ok(message)
        } catch (error) {
            console.log(error)
            fail(message)
        }
    }
}

export {
    Retrive
}