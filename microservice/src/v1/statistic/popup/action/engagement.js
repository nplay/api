
import Conector from '../../../../conector'
import HistoryModel from '../../../../../../src/history/model/HistoryModel'

export default {
    params: {
        identifier: { type: "string" },

        startDate: { type: "string" },
        endDate: { type: "string" }
    },

    async handler(ctx) {
        let params = ctx.params

        let conn = new Conector().connect(params.identifier)
        
        return await HistoryModel(conn).aggregate([
            {
                $unwind: "$prospectItens"
            },
            {
                $unwind: "$prospectItens.originCollection"
            },
            {
                $match: {
                   "prospectItens.originCollection.node.id": "popup"
                }
            },
            {
                $group: {
                    _id: "$prospectItens.originCollection.id",
                    title: { $first: "$prospectItens.originCollection.title" },
                    total: { $sum: 1 }
                }
            }
        ])
    }
}