
import engagement from './action/engagement'

export default {
    name: "statistic.popup",
    version: 1,
    methods: {
    },
    actions: {
        engagement
    },
    events: {
    }
}