
import HistoryModel from '../../../../../../src/history/model/HistoryModel'
import Conector from '../../../../conector'

export default {
    params: {
        identifier: { type: "string" },

        startDate: { type: "string" },
        endDate: { type: "string" }
    },

    async handler(ctx) {
        let params = ctx.params

        let conn = new Conector().connect(params.identifier)

        return await HistoryModel(conn).aggregate([
            {
                $match: {
                    "created_at": {
                        $gte: new Date(params.startDate),
                        $lt: new Date(params.endDate)
                    }
                }
            },
            {
                $project: {
                    mobile: {
                        $arrayElemAt: ["$userAgent.mobile", -1]
                    }
                }
            },
            {
                $group: {
                    _id: "$mobile",
                    mobile: {
                        $first: "$mobile"
                    },
                    total: {
                        $sum: 1
                    }
                }
            },
            {
                $project: {
                    _id: 0
                }
            }
        ])
    }
}