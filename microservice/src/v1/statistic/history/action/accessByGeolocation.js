
import HistoryModel from '../../../../../../src/history/model/HistoryModel'
import Conector from '../../../../conector'

export default {
    params: {
        identifier: { type: "string" },

        startDate: { type: "string" },
        endDate: { type: "string" }
    },

    async handler(ctx) {
        let params = ctx.params

        let conn = new Conector().connect(params.identifier)

        return await HistoryModel(conn).aggregate([
            {
                $match: {
                    "created_at": {
                        $gte: new Date(params.startDate),
                        $lt: new Date(params.endDate)
                    },
                    "geolocation.0": {
                        $exists: true
                    }
                }  
            },
            
            {
                $project: {
                    geolocation: {
                        $arrayElemAt: [ "$geolocation", -1 ]
                    }
                }
            },
            
            {
                $group: {
                    _id: {
                        city: "$geolocation.city",
                        region: "$geolocation.region",
                        countryCode: "$geolocation.countryCode"
                    },
                    total: {
                        $sum: 1
                    }
                }
            },
            
            {
                $project: {
                    _id: 0,
                    total: 1,
                    geolocation: "$_id"
                }
            }
        ])
    }
}