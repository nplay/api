
import accessByDevice from './action/accessByDevice'
import accessByGeolocation from './action/accessByGeolocation'

export default {
    name: "statistic.history",
    version: 1,
    methods: {
    },
    actions: {
        accessByDevice,
        accessByGeolocation
    },
    events: {
    }
}