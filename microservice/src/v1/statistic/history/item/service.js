
import accessByHour from './action/accessHour'

export default {
    name: "statistic.history.item",
    version: 1,
    methods: {
    },
    actions: {
        accessByHour
    },
    events: {
    }
}