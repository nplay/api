
import HistoryModel from '../../../../../../../src/history/model/HistoryModel'
import Conector from '../../../../../conector'

export default {
    params: {
        identifier: { type: "string" },

        startDate: { type: "string" },
        endDate: { type: "string" }
    },

    async handler(ctx) {
        let params = ctx.params

        let conn = new Conector().connect(params.identifier)

        return await HistoryModel(conn).aggregate([
            {
                $match: {
                    "created_at": {
                        $gte: new Date(params.startDate),
                        $lt: new Date(params.endDate)
                    },
                    "itens.accessCollection.0": { $exists: true }
                }
            },

            { $sort : { 'itens.accessCollection.ocurrence' : -1 } },
            
            {
                $unwind: "$itens"
            },
            
            {
                $unwind: "$itens.accessCollection"
            },

            { $limit : 30 },
            
            {
                $project: {
                    title: "$itens.product.title",
                    entityId: "$itens.product.entityId",
                    ocurrence: "$itens.accessCollection.ocurrence",
                    accessDate: {
                        $dateToString: {
                            format: '%Y-%m-%d %H',
                            date: "$itens.accessCollection.date"
                        }
                    }
                }
            },
            
            {
                $group: {
                    _id: {
                        entityId: "$entityId",
                        accessDate: "$accessDate"
                    },
                    ocurrence: {
                        $sum: "$ocurrence"
                    },
                    title: { $first: '$title' }
                }
            },
            
            {
                $project: {
                    _id: 0,
                    title: 1,
                    accessDate: '$_id.accessDate',
                    entityId: "$_id.entityId",
                    total: "$ocurrence"
                }
            }
        ])
    }
}