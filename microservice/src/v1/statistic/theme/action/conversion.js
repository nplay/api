
import Conector from '../../../../conector'
import AnalyticsOrderModel from '../../../../../../src/analytics/model/OrderModel'

class Wrapper
{
    async get({identifier, startDate, endDate})
    {
        let conn = new Conector().connect(identifier)
        return await AnalyticsOrderModel(conn).aggregate([
            {
                $match: {
                    "items.originCollection.node.id": "theme",
                    "created_at": {
                        $gte: new Date(startDate),
                        $lt: new Date(endDate)
                    }
                }
            },
            
            {
                $unwind: "$items"
            },
            
            {
                $unwind: "$items.originCollection"
            },
            
            {
                $match: {
                    "items.originCollection.node.id": "theme"
                }
            },
            
            {
                $group: {
                    _id: "$items.originCollection.id",
                    title: { $first: "$items.originCollection.title" },
                    qty: { $sum: 1 }
                }
            }
        ])
    }
}

export default {
    params: {
        identifier: { type: "string" },

        startDate: { type: "string" },
        endDate: { type: "string" }
    },

    async handler(ctx) {
        let params = ctx.params

        return await new Wrapper().get(params)
    }
}