
import accessVsSell from './action/accessVsSell'

export default {
    name: "statistic.analytics",
    version: 1,
    methods: {
    },
    actions: {
        accessVsSell
    },
    events: {
    }
}