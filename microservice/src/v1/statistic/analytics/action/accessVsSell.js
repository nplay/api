
import Conector from '../../../../conector'
import AnalyticsOrderModel from '../../../../../../src/analytics/model/OrderModel'
import HistoryModel from '../../../../../../src/history/model/HistoryModel'
import { start } from 'pm2'

class Wrapper
{
    constructor(conn)
    {
        this.conn = conn
    }

    async analyticsAccess(startDate, endDate)
    {
        return await AnalyticsOrderModel(this.conn).aggregate([
            {
                $match: {
                    created_at: {
                        $gt: new Date(startDate),
                        $lt: new Date(endDate),
                    }
                }
            },
            
            {
                $group: {
                    _id: {
                        $dateToString: {
                            format: "%Y-%m-%d",
                            date: "$created_at"
                        }
                    },
                    total: {
                        $sum: 1
                    }
                }
            },
            
            {
                $sort: {
                    date: -1
                }  
            },
            
            {
                $project: {
                    _id: 0,
                    total: 1,
                    day: "$_id"
                }
            }
        ])
    }

    async historyAccess(startDate, endDate)
    {
        return await HistoryModel(this.conn).aggregate([
            {
                $match: {
                    created_at: {
                        $gt: new Date(startDate),
                        $lt: new Date(endDate),
                    }
                }
            },
            
            {
                $group: {
                    _id: {
                        $dateToString: {
                            format: "%Y-%m-%d",
                            date: "$created_at"
                        }
                    },
                    total: {
                        $sum: 1
                    }
                }
            },
            
            {
                $sort: {
                    date: -1
                }  
            },
            
            {
                $project: {
                    _id: 0,
                    total: 1,
                    day: "$_id"
                }
            }
        ])
    }

    async merge(history, analytics, startDate, endDate)
    {
        //Absolute date
        startDate = new Date(startDate)
        endDate = new Date(endDate)

        startDate = new Date(`${startDate.getFullYear()}-${startDate.getMonth()+1}-${startDate.getDate()}`)
        endDate = new Date(`${endDate.getFullYear()}-${endDate.getMonth()+1}-${endDate.getDate()}`)

        let oneDayMs = 86400000

        let time = endDate.getTime() - startDate.getTime()
        let seconds = time / 1000
        let hours =  (seconds / 60) / 60
        let days = hours / 24

        let output = new Array

        for(let day = 0; day < days; ++day)
        {
            let subtractDay = oneDayMs * day
            let curDate = new Date(endDate.getTime() - subtractDay)

            let curMonth = curDate.getMonth()+1 < 10 ? '0'+(curDate.getMonth()+1) : curDate.getMonth()+1
            let curDay = curDate.getDate() < 10 ? '0'+curDate.getDate() : curDate.getDate()
            let dateString = `${curDate.getFullYear()}-${curMonth}-${curDay}`

            let data = {
                total: 0
            }

            let access = history.find(item => item.day == dateString) || data
            let sell = analytics.find(item => item.day == dateString) || data
            
            output.push({
                day: dateString,
                access: access.total,
                sell: sell.total
            })
        }

        return output
    }
}

export default {
    params: {
        identifier: { type: "string" },

        startDate: { type: "string" },
        endDate: { type: "string" }
    },

    async handler(ctx) {
        let params = ctx.params
        let conn = new Conector().connect(params.identifier)
        let wrapper = new Wrapper(conn)
        let historyAccess = await wrapper.historyAccess(params.startDate, params.endDate)
        let analyticsAccess = await wrapper.analyticsAccess(params.startDate, params.endDate)

        return await wrapper.merge(historyAccess, analyticsAccess, params.startDate, params.endDate)
    }
}