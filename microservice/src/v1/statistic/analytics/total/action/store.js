
import Conector from '../../../../../conector'
import AnalyticsOrderModel from '../../../../../../../src/analytics/model/OrderModel'

export default {
    params: {
        identifier: { type: "string" },

        startDate: { type: "string" },
        endDate: { type: "string" }
    },

    async handler(ctx) {
        let params = ctx.params

        let conn = new Conector().connect(params.identifier)

        return await AnalyticsOrderModel(conn).aggregate([
            {
                $unwind: "$items"
            },
            
            {
                $match: {
                    created_at: {
                        $gt: new Date(params.startDate),
                        $lt: new Date(params.endDate),
                    },
                    "items.originCollection.0": {
                        $exists: false
                    }
                }
            },
            
            {
                $group: {
                    _id: null,
                    total: {
                        $sum: {
                            $multiply: ["$items.price", "$items.quantity"]
                        }
                    },
                    ordersQty: {
                        $sum: 1
                    }
                }
            },
            
            {
                $project: {
                    _id: 0,
                    total: 1,
                    ordersQty: 1
                }
            }
        ])
    }
}