
import Conector from '../../../../../conector'
import AnalyticsOrderModel from '../../../../../../../src/analytics/model/OrderModel'

export default {
    params: {
        identifier: { type: "string" },

        startDate: { type: "string" },
        endDate: { type: "string" }
    },

    async handler(ctx) {
        let params = ctx.params

        let conn = new Conector().connect(params.identifier)

        return await AnalyticsOrderModel(conn).aggregate([
            {
                $unwind: "$items"
            },
            
            {
              $unwind: "$items.originCollection"  
            },
            
            {
                $match: {
                    created_at: {
                        $gt: new Date(params.startDate),
                        $lt: new Date(params.endDate),
                    },
                    "items.originCollection.node.id": "theme"
                }
            },
            
            {
                $group: {
                    _id: "$items.originCollection.id",
                    title: {
                        $first: "$items.originCollection.title"
                    },
                    total: {
                        $sum: {
                            $multiply: ["$items.price", "$items.quantity"]
                        }
                    }
                }
            }
          
        ])
    }
}