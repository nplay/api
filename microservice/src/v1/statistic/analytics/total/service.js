
import theme from './action/theme'
import store from './action/store'

export default {
    name: "statistic.analytics.total",
    version: 1,
    methods: {
    },
    actions: {
        theme,
        store
    },
    events: {
    }
}