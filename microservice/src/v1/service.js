
import APIService from './api/service'

import AnalyticsService from './analytics/service'
import StorageService from './storage/service'
import StatisticThemeService from './statistic/theme/service'
import StatisticHistoryService from './statistic/history/service'
import StatisticHistoryItemService from './statistic/history/item/service'
import StatisticPopupService from './statistic/popup/service'
import StatisticAnalyticsService from './statistic/analytics/service'
import StatisticAnalyticsTotalService from './statistic/analytics/total/service'
import UserService from './user/service'
import UserStatusService from './userStatus/service'
import ThemeSiteService from './themes/site/service'
import ThemePopupService from './themes/popup/service'
import CategoryService from './category/service'

export default async (broker) => {

    broker.createService(APIService)
    broker.createService(StorageService)
    broker.createService(AnalyticsService)
    broker.createService(StatisticThemeService)
    broker.createService(StatisticHistoryService)
    broker.createService(StatisticHistoryItemService)
    broker.createService(StatisticPopupService)
    broker.createService(StatisticAnalyticsTotalService)
    broker.createService(StatisticAnalyticsService)
    broker.createService(UserService)
    broker.createService(UserStatusService)

    broker.createService(ThemeSiteService)
    broker.createService(ThemePopupService)
    broker.createService(CategoryService)

    //broker.createService(HistoryService);
}