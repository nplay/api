
import { UserModel } from '../../../../../src/user/model/UserModel'
import { StatusStoreEntity } from '../../Entity/StatusStoreEntity'
import Conector from '../../../conector'

class InsertStatusStore
{
    /**
     * @param {StatusStoreEntity} statusStoreEntity
     */
    async execute(identifier, statusStoreEntity)
    {
        let conn = new Conector().connectCore()

        let user = await UserModel(conn).findOne({
            'stores.database': identifier
        })

        if(!user)
            throw new Error(`User not found with identifier stores.database '${identifier}' `)

        let store = user.stores.find(store => { return store.database == identifier})

        if(!store)
            throw new Error(`Store not found with identifier stores.database '${identifier}'`)

        let idx = store.status.findIndex(status => { return status.code == statusStoreEntity.code})

        if(idx > -1)
            store.status = store.status.splice(idx+1, 1)

        store.status.push(statusStoreEntity)
        
        await UserModel(conn).updateOne({
            'stores.database': identifier
        }, user)
    }
}

export {
    InsertStatusStore
}