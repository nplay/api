
import get from './action/get'
import insert from './action/insert'

const Event2 = require("event2");

export default {
    name: "user.status",
    version: 1,
    mixins: [Event2],
    methods: {
    },
    actions: {
        get,
        insert
    }
}