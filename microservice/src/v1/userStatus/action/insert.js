
import { StatusStoreEntity, StatusStoreStatusEnum } from '../../Entity/StatusStoreEntity'
import { InsertStatusStore } from '../method/insertStatusStore'

export default {
    params: {
        identifier: {
            type: "string"
        },
        code: {
            type: "number",
        },
        technicalData: {
            type: "string"
        },
        status: {
            type: "string"
        },
        updated_at: {
            type: "string"
        }
    },
    async handler(ctx) {
        
        let identifier = ctx.params.identifier

        let statusStoreEntity = new StatusStoreEntity()
        await statusStoreEntity.setCode(ctx.params.code)
        await statusStoreEntity.setTechnicalData(ctx.params.technicalData)
        await statusStoreEntity.setUpdatedAt(new Date(ctx.params.updated_at))

        let statusEnum = await new StatusStoreStatusEnum().set(ctx.params.status)
        await statusStoreEntity.setStatus(statusEnum)

        let insertStatus = new InsertStatusStore()
        await insertStatus.execute(identifier, statusStoreEntity)

        return await statusStoreEntity
    }
}