
import StoreModel from '../../../../../src/user/model/StoreModel'
import Conector from '../../../conector'

export default {
    params: {
        identifier: {
            type: "string"
        }
    },
    async handler(ctx) {
        let identifier = ctx.params.identifier
        let conn = new Conector().connect(identifier)
        
        let storeModel = await StoreModel(conn)
        return await storeModel.find()
    }
}