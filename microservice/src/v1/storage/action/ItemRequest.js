
export default {
    params: {
        identifier: { type: "string" }
    },
    async handler(ctx) {

        let identifier = ctx.params.identifier
        if(!identifier)
            throw new Error(`Identifier not found`)

        await this.itemRequest(identifier)
    }
}
