import Conector from '../../../conector'
import ItemModel from '../../../../../src/storage/model/ItemModel'

export default {
    params: {
        identifier: { type: "string" }
    },
    async handler(ctx) {
        let filters = ctx.params.filter || {}
        let query = ctx.params.query || {}

        let identifier = ctx.params.identifier

        if(!identifier)
            throw new Error(`Identifier not found`)

        let conn = new Conector().connect(identifier)
        let itens = await ItemModel(conn).find(filters, query)

        return {
            itens: itens
        }
    }
}