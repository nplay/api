
import Conector from '../../../conector'
import ItemModel from '../../../../../src/storage/model/ItemModel'

export default {
    params: {
        identifier: { type: "string" }
    },
    async handler(ctx) {

        let conn = new Conector().connect(ctx.params.identifier)

        let total = await ItemModel(conn).find().count()
        return total
    }
}