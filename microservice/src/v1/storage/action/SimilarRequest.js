
export default {
    params: {
        identifier: { type: "string" }
    },
    async handler(ctx) {

        let identifier = null

        try {
            identifier = ctx.params.identifier

            if(!identifier)
                throw new Error(`Identifier not found`)
    
            await this.itemSimilarRequest(identifier)
            return true
        } catch (error) {
            this.emit2({}, 'v1.any.fatalError', {
                identifier: identifier,
                context: 'Action ItemSimilarRequest',
                subject: `${identifier} solicitação falhou`,
                error: error.message,
                attachments: [
                    {
                        filename: 'data.log',
                        content: JSON.stringify({
                            identifier: identifier,
                            data: {}
                        }),
                        contentType: 'text/plain'
                    },
                    {
                        filename: 'stackTrace.log',
                        content: error.stack,
                        contentType: 'text/plain'
                    }
                ]
            })

            throw new Error(error.message)
        }
    }
}
