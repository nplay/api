
import Count from './action/Count'
import SimilarRequest from './action/SimilarRequest'

import {SimilarResult} from './listener/SimilarResult';
import ItemRequest from './action/ItemRequest';
import ItemRequestAll from './action/ItemRequestAll';
import SimilarRequestAll from './action/SimilarRequestAll';
import ViewToViewRequest from './action/ViewToViewRequest';
import ViewToViewRequestAll from './action/ViewToViewRequestAll';
import Find from './action/Find';

import { ItemRequestMethod } from './method/ItemRequest'
import { ItemSimilarRequestMethod } from './method/ItemSimilarRequest'
import { ViewToViewRequestMethod } from './method/ViewToViewRequest';
import { ViewToViewResult } from './listener/ViewToViewResult';

const Event2 = require("event2");
const Cron = require("moleculer-cron");

export default {
    name: "storage",
    version: 1,
    mixins: [Event2, Cron],
    methods: {
        itemRequest: ItemRequestMethod,
        itemSimilarRequest: ItemSimilarRequestMethod,
        viewToViewRequest: ViewToViewRequestMethod
    },
    actions: {
        find: Find,
        count: Count,
        similarRequest : SimilarRequest,
        similarRequestAll: SimilarRequestAll,
        itemRequest: ItemRequest,
        itemRequestAll: ItemRequestAll,
        viewToViewRequest: ViewToViewRequest,
        viewToViewRequestAll: ViewToViewRequestAll
    },

    events2: [
        {
            event:'v1.ipapi.similarResult',
            listeners: {
                retrive: {
                    handle: new SimilarResult().execute,
                    createDeadLetter: false
                }
            }
        },
        {
            event:'v1.data-mining.viewToViewRequest',
            listeners: {
                retrive: {
                    handle: new ViewToViewResult().execute,
                    createDeadLetter: false
                }
            }
        }
    ],

    crons: [
        {
            name: "ItemRequestAllSchedule",
            cronTime: '*/10 * * * *',
            onTick: function() {
                this.call('v1.storage.itemRequestAll')
            },
            //runOnInit: function() {},
            timeZone: 'America/Sao_Paulo'
        },
        {
            name: "ItemSimilarRequestAllSchedule",
            //cronTime: '* */5 * * * *',
            cronTime: '1 */6 * * *',
            onTick: function() {
                this.call('v1.storage.similarRequestAll')
            },
            //runOnInit: function() {},
            timeZone: 'America/Sao_Paulo'
        },
        {
            name: "ItemViewToViewRequestAll",
            //cronTime: '* */5 * * * *',
            cronTime: '1 */6 * * *',
            onTick: function() {
                //this.call('v1.storage.viewToViewRequestAll')
            },
            //runOnInit: function() {},
            timeZone: 'America/Sao_Paulo'
        }
    ]
}