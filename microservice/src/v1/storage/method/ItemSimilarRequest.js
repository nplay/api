
import Conector from '../../../conector'
import ItemModel from '../../../../../src/storage/model/ItemModel'

let ItemSimilarRequestMethod = {
    async handler(identifier) {

        if(!identifier)
            throw new Error(`Identifier is not defined`)

        let conn = new Conector().connect(identifier)

        let itens = await ItemModel(conn).find({
            allowed: true,
            title: { "$ne": ""}
        }, {
            _id: 0,
            id: 1,
            title: 1
        })

        this.emit2({}, 'v1.storage.similarRequest', {
            identifier: identifier,
            itens: itens
        })
    }
}

export {
    ItemSimilarRequestMethod
}
