
import Conector from '../../../conector'
import {UserModel} from '../../../../../src/user/model/UserModel'

let ItemRequestMethod = {
    async handler(identifier) {
        let conn = new Conector().connectCore()

        let userModel = await UserModel(conn)
        let output =  await userModel.findOne({'stores.database': identifier}, {'stores.$': 1, _id: 0})

        if(!output)
            throw new Error(`Store not found`)

        let store = output.stores[0]

        this.emit2({}, 'v1.storage.itemRequest', {
            identifier: identifier,
            store: store
        })
    }
}

export {
    ItemRequestMethod
}
