
import Conector from '../../../conector'
import HistoryModel from '../../../../../src/history/model/HistoryModel'

let ViewToViewRequestMethod = {
    async handler(identifier) {

        if(!identifier)
            throw new Error(`Identifier is not defined`)

        let conn = await new Conector().connect(identifier)
        let sessions = await HistoryModel(conn).find({}, {"itens.product.id": 1})

        let sessionItens = new Array

        for(let session of sessions)
        {
            let itens = new Array
    
            for(let item of session.itens)
            {   
                itens.push(item.product.id)
    
                if(itens.length == 4)
                    break;
            }
    
            if(itens.length > 0)
                sessionItens.push(itens)
        }

        this.emit2({}, 'v1.storage.viewToViewRequest', {
            identifier: identifier,
            itens: sessionItens
        })
    }
}

export {
    ViewToViewRequestMethod
}