import Conector from '../../../conector'
import SimilarMiningModel from '../../../../../src/mining/similar/model/similarModel'

class SimilarResult
{
    async execute(content, message, ok, fail){
        let body = content.params

        let itens = body.itens
        let identifier = body.identifier

        try {
           if(!itens || !identifier)
                throw new Error(`Itens or identifier not defined`)

            let conn = await new Conector().connect(identifier)
    
            //Delete all
            await SimilarMiningModel(conn).remove({})
            await SimilarMiningModel(conn).insertMany(itens)

            ok(message)
        } catch (error) {

            this.emit2({}, 'v1.any.fatalError', {
                identifier: identifier,
                context: 'Listener v1.ipapi.similarResult',
                subject: `${identifier} solicitação falhou`,
                error: error.message,
                attachments: [
                    {
                        filename: 'data.log',
                        content: JSON.stringify({
                            identifier: identifier,
                            data: body
                        }),
                        contentType: 'text/plain'
                    },
                    {
                        filename: 'stackTrace.log',
                        content: error.stack,
                        contentType: 'text/plain'
                    }
                ]
            })

            fail(message)
        }
    }
}

export {
    SimilarResult
}