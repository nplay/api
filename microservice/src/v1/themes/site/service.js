
import initialThemesListener from './listener/initialThemes'

const Event2 = require("event2");

export default {
    name: "theme.site",
    version: 1,
    mixins: [Event2],
    methods: {
    },
    events2: [
        {
            event: 'v1.user.newUserAfter',
            listeners: {
                initialThemes: {
                    handle: initialThemesListener
                }
            }
        }
    ],
    actions: {
    }
}