
import Conector from '../../../../conector'

export default async function(content, message, ok, fail){
    try {
        let platform = content.params.store.platform
        let curDB = content.params.store.database

        let pathSeed = `${__dirname}/../seed/${platform}`
        await new Conector().runSeed(pathSeed, curDB)

        ok(message)
    } catch (error) {
        console.log(error)
        fail(message)
    }
}