import HistoryModel from "../../../../src/history/model/HistoryModel"

import Conector from '../../conector'

export default async (historyModel, identifier) => {

    let conn = new Conector().connection(identifier)

    await HistoryModel(conn).updateOne({
        sessionID: historyModel.sessionID
    }, {
        $set: {
            'created_at': historyModel.created_at,
            'prospectItens': historyModel.prospectItens,
            'itens': historyModel.itens,
            'userAgent': historyModel.userAgent,
            'weather': historyModel.weather,
            'geolocation': historyModel.geolocation
        }
    }, { upsert: true })
}