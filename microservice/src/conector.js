import {Config} from 'core'
import { Seeder } from 'mongo-seeding'

var mongoose = require("mongoose")

class Conector
{
    constructor()
    {
    }

    get instances()
    {
        if(!Conector._instances)
            Conector._instances = new Object

        return Conector._instances
    }

    connection(name)
    {
        let instances = this.instances

        if(!instances.hasOwnProperty(name))
            instances[name] = this.createConnection(name)
    
        return instances[name]
    }

    async runSeed(path, customDB)
    {
        let configDB = Config.database

        let conStr = `mongodb://${configDB.host}:${configDB.port}/${customDB}`

        let config = {
            database: conStr,
            dropDatabase: false,
            dropCollections: true
        };

        let conn = this.connect(customDB)

        let seeder = new Seeder(config);
        let collections = seeder.readCollectionsFromPath(path);

        for(let c of collections)
        {
            let dbCollection = conn.collection(c.name)

            await dbCollection.insert(c.documents)
        }
    }

    connectCore()
    {
        return this.connect(Config.database.db);
    }

    createConnection(name)
    {
        let db = Config.database

        let connString = `mongodb://${db.host}:${db.port}/${name}`
        return mongoose.createConnection(connString)
    }

    connect(db)
    {
        return this.connection(db)
    }
}

export default Conector