const { ServiceBroker } = require("moleculer");
import {Config} from "core"

global.broker = new ServiceBroker({
    nodeID: process.env.HOSTNAME,
    transporter: Config.queue.driver.rabbitMQ.connectionString
});

import themesMenuService from './themes/menu/service'
import V1Service from './v1/service'

import {
    AfterSaveHistoryMetricListener,
    HistorySaveAfter,
}
from './listeners'

const Event2 = require("event2");

let fnRun = async () => {

    await V1Service(broker)
    await broker.createService(themesMenuService)

    broker.createService({
        name: "api",
        version: 1,
        mixins: [Event2],
        events2: [
            {
                event: "v1.history.updateAfter",
                listeners: {
                    updateHistory: {
                        handle: async (content, message, ok, fail) => {
                            try {
                                await HistorySaveAfter(content.params.history, content.params.identifier)
                                ok(message)
                            } catch (error) {
                                console.log(error)
                                fail(message)
                            }
                        }
                    }
                }
            },
            {
            event: 'v1.history.update-apiBefore',
            listeners: {
                categoryAccess: {
                    handle: async (content, message, ok, fail) => {
                        //Track-viewed item event
                        try {
                            await AfterSaveHistoryMetricListener(
                                content.params.history,
                                content.params.identifier
                            )
    
                            ok(message)
                        } catch (error) {
                            console.log(error)
                            fail(message)
                        }
                    }
                },
                updateHistory: {
                    handle: async (content, message, ok, fail) => {
                        try {
                            await HistorySaveAfter(content.params.history, content.params.identifier)
                            ok(message)
                        } catch (error) {
                            console.log(error)
                            fail(message)
                        }
                    }
                }
            }
        }]
    });

    await broker.start()
}

fnRun()