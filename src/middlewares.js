import PermissionMiddleware from './middleware/permissionMiddleware'

let attachMiddlewares = async (middlewares, req, res, additData) => {
	let authorized = true

	for(let midClass of middlewares)
	{
		let middleware = new midClass(res, req, additData)
		let response = await middleware.run()

		if(!response)
		{
			authorized = false
			await middleware.resFail()
			break
		}
	}

	return authorized
}

export default (args) => {
	args.app.use(function(req, res, next)
	{
		res.setTimeout(2000, function(){
			console.log('TIMEOUT ', + new Date().getTime())
		});
	
		let fnProcess = async () => {
			try {
				args.httpContext.set('token', req.header('Authorization-token'))
				let authorized = await attachMiddlewares([PermissionMiddleware], req, res, {})
			
				if(!authorized)
					return false

				await next();
					
			} catch (error) {
				console.log(error)	
				next(error)
			}
		}
	
		fnProcess()
	});
}