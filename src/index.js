process.env.TZ = 'America/Sao_Paulo'

global.express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors')
var httpContext = require('express-http-context');

global.Conector = require("./database/mongodb/connector.js").Conector;
global.ResponseHelper = require("./helper/ResponseHelper").ResponseHelper
var app = express();

//ENABLE ALL CROSS-ORIGIN
app.use(cors())

//PARSE BODY TO JSON DATA
app.use(bodyParser.json({
	limit: '2mb'
}));
app.use(bodyParser.urlencoded({extended:true}))

//PREVENT INJECTION IN QUERIES
var filter = require('content-filter')
app.use(filter({dispatchToErrorHandler: true, urlMessage: "Expression blocked in request"}))

//USE HTTP-CONTEXT
app.use(httpContext.middleware);

/**
 * MIDDLEWARE.
 */
import middlewares from './middlewares'
middlewares({app: app, httpContext: httpContext})

/**
 * Routes
 */
import routes from './routes'
routes(app)

/**
 * SENTRY.IO CONFIGURATION
 */
//const Sentry = require('@sentry/node');
//Sentry.init({ dsn: 'https://44aca30a4d024cc0be1739a91debb451@sentry.io/1451382' });
//app.use(Sentry.Handlers.requestHandler());

/**
 * MANIPULADOR GLOBAL DE ERROS (SENTRY.IO)
 */
//SENTRY HANDLE
//app.use(Sentry.Handlers.errorHandler());

app.use(function onError(err, req, res, next) {
	// The error id is attached to `res.sentry` to be returned
	// and optionally displayed to the user for support.

	console.error('MANIPULADOR GLOBAL DE ERROS', err)

	if(typeof err == 'object' && err.code == 'FORBIDDEN_CONTENT')
		err = new Error(`${err.message} | ${err.code}`)

	ResponseHelper.setError(res,`Erro critico de sistema ${err.toString()}, não se preocupe a 
	falha foi enviada para nossos servidores de monitoramento`)
});

global.server = app.listen(3000, function () {
	console.log('Running!');
});