var mongoose = require('mongoose');
var schema = new mongoose.Schema(
    {
        id: {
            type: String,
            required: true
        },

        ids: []
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class Class
{
    static async test()
    {
    }
}

schema.loadClass(Class);
let nameModel = "MiningSimilar";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    return conn.model(nameModel, schema)
}