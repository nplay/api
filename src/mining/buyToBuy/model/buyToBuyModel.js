var mongoose = require('mongoose');
var schema = new mongoose.Schema(
    {
        from: {
            type: String,
            required: true
        },

        to: {
            type: Array,
            default: []
        },

        support: {
            type: Number,
            required: true
        },

        lift: {
            type: Number,
            required: true
        },

        confidence:  {
            type: Number,
            required: true
        }
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class Class
{
    static async test()
    {
    }
}

schema.loadClass(Class);
let nameModel = "MiningBuyToBuy";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    return conn.model(nameModel, schema)
}