
class ResponseHelper
{
    setSuccess(res, message)
    {
        this._set(res, 200, message);
    }

    setError(res, message)
    {
        this._set(res, 500, message);
    }

    setUnauthorized(res, message)
    {
        this._set(res, 403, message);
    }

    setWarning(res, message)
    {
        this._set(res, 400, message);
    }

    _set(res, status, message)
    {
        let responseData = {
            'success': status == 200,
            'data': message
        };

        res.status(status).send(responseData);
    }
}

module.exports = {
    ResponseHelper: new ResponseHelper()
}