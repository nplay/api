import { UserModel } from "../user/model/UserModel"

class UserHelper {
	constructor() {
		this.conn = Conector.connectCore()
	}

	async storeByPublicKeys(publicKeys) {
		let stores = await UserModel(this.conn).storeByPublicKeys(publicKeys)
		return stores.length > 0 ? stores[0] : null
	}

	async getUser(database = null) {
		database = database || await Conector.getDB()
		return UserModel(this.conn).findOne({ "stores.database": database })
	}

	async storeByDB(database = null)
	{
		database = database || await Conector.getDB()
		let stores = await UserModel(this.conn).storeByDB(database)
		return stores.length > 0 ? stores[0] : null
	}
}

module.exports = UserHelper