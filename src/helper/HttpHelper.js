var request = require('request');

/**
 * DEPRECATED USE AXIOS
 */
class HttpHelper
{
    constructor()
    {
        console.log('HttpHelper DEPRECATED USE AXIOS')
    }

    get(pHeaders, pUrl, pBody, callback)
    {
        request.get({
            headers: Object.assign({
                "content-type": "application/json"
            }, pHeaders),
            url: pUrl,
            json: true,
            body: pBody
          },
          function(error, response, body){
            callback(error, response, body);
          }
        );
    }
}

module.exports = {
    HttpHelper: HttpHelper
}