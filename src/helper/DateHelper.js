
class DateHelper
{
    constructor()
    {
        this.zone = "America/Sao_Paulo"
        this.moment = require('moment-timezone');
    }

    toTimezone(date)
    {
        date = date == undefined ? new Date() : date
        let zone = this.moment.tz(date, this.zone);
        let objDate = zone.toDate()
        return objDate
    }

    diff(startDate, endDate, mask)
    {
        let start = this.moment(startDate);
        let end = this.moment(endDate);
        mask = mask == undefined ? 'days' : mask

        return end.diff(start, mask)
    }

    subtract(days, date)
    {
        date = date == undefined ? new Date() : date
        return this.moment(date).subtract(days, 'days').toDate()
    }
}

export default DateHelper