
const { readdirSync, readFileSync } = require('fs')
const getDirectories = source =>
  readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent)

const getFiles = source =>
    readdirSync(source, { withFileTypes: true })
      .filter(dirent => dirent.isFile())
      .map(dirent => dirent)
    
var router = express.Router();
import UserHelper from '../../../helper/UserHelper'

/**
 * GET DEFAULT THEMES
 */
router.route('/:typeTheme?')
.get(function(req, res)
{
    let fnProcess = async () => {
        let typeTheme = req.params.typeTheme

        let entity = await new UserHelper().storeByDB()
        let themesCollection = new Array

        let pathSeed = `${__dirname}/../seed/${entity.store.platform}`
        let dirs = getDirectories(pathSeed)
        
        for(let dir of dirs)
        {
            let pathDir = `${pathSeed}/${dir.name}`
            let files = getFiles(pathDir)
            
            for(let file of files)
            {
                let pathFile = `${pathDir}/${file.name}`
                let themes = JSON.parse(readFileSync(pathFile).toString()).filter(theme => {
                    return typeTheme ? theme.additionalData.typeTheme == typeTheme : true
                })

                themesCollection = themesCollection.concat(themes)
            }
        }
        
        return themesCollection
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})

module.exports = router;