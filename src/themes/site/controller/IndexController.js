var router = express.Router();

import PlanResourceMiddleware from '../../../middleware/planResourceMiddleware'
import ThemeModel from "../model/ThemeModel"

router.use(async (req, res, next) => {

    let resourceMidleware = new PlanResourceMiddleware(res, req, {
        resource: PlanResourceMiddleware.RESOURCE_VITRINE
    })

    let middlewares = [resourceMidleware]

    for(let middleware of middlewares)
    {
       if(!await middleware.run())
        return await middleware.resFail()
    }

    return next()
})

router.route('/')
.post(function(req, res)
{
    let fnProcess = async () => {
        if(Object.keys(req.body.attributes).length > 0)
            return await ThemeModel().find(req.body.filters, req.body.attributes)
        else
            return await ThemeModel().find(req.body.filters)
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})
.put(function(req, res)
{
    let fnProcess = async () => {
        return await new ThemeModel()(req.body).save()
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
});

router.route('/:EntityID')
.patch(function(req, res)
{
    let fnProcess = async () => {
        let entityID = req.params.EntityID

        let response = await ThemeModel().replaceOne({_id: entityID}, req.body);

        if(!response.ok)
            return -1
        
        return response.nModified == 1 ? await ThemeModel().findOne({_id: entityID}) : null
    }

    fnProcess().then(response => {
        if(response == -1)
            ResponseHelper.setError(res, 'Object not found or not modified')

        if(response != null)
            ResponseHelper.setSuccess(res, response)
        else
            ResponseHelper.setWarning(res, 'Nada foi alterado')
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})
.delete(function(req, res)
{
    let fnProcess = async () => {
        let entityID = req.params.EntityID
        return await ThemeModel().findOneAndRemove({_id: entityID});
    }
    
    fnProcess().then(response => {

        if(response == null)
            ResponseHelper.setError(res, 'Object not found')
        else
            ResponseHelper.setSuccess(res, 'OK')
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})

module.exports = router;