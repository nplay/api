var mongoose = require('mongoose');
import {Conector} from "../../../database/mongodb/connector"

var schema = new mongoose.Schema(
    {
        html: {
            type: String,
            required: true
        }
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class Class
{
    static async test()
    {
    }
}

schema.loadClass(Class);
let nameModel = "ThemesSiteDesign";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}