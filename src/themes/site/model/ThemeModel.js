var mongoose = require('mongoose');
import ThemeLineModel from "../model/ThemeLineModel"
import DesignModel from "../model/DesignModel"

var schema = new mongoose.Schema(
    {
        enabled: {
            type: Boolean,
            default: false,
            required: false
        },

        name: {
            type: String,
            required: true
        },

        xpath: {
            type: String,
            required: true
        },

        allowDrop: {
            type: Boolean,
            required: true,
            default: false
        },

        additionalData: {
            type: Object,
            required: false,
            default: null
        },

        maxItens: {
            type: Number,
            required: true,
            default: 15
        },

        swiper: {
            slidesPerView: {
                type: Number,
                default: 0
            },
        },

        designModel: {
            type: DesignModel().schema,
            required: true
        },

        themeLineCollection: [ThemeLineModel().schema]
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class Class
{
    static async test()
    {
    }
}

schema.loadClass(Class);
let nameModel = "ThemesSiteTheme";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}