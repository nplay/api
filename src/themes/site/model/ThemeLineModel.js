var mongoose = require('mongoose');
import {Conector} from "../../../database/mongodb/connector"
import ComponentModel from "../model/ComponentModel"

var schema = new mongoose.Schema(
    {
        isPreview: {
            type: Boolean,
            default: false,
            required: false
        },

        enabled: {
            type: Boolean,
            default: true
        },

        allowDrop: {
            type: Boolean,
            required: true,
            default: false
        },

        additionalData: {
            categoryID: {
                type: Number,
                required: false,
                default: null
            },

            currentOption: {
                type: String,
                required: false,
                default: null
            },

            currentFilter: {
                type: String,
                required: false,
                default: null
            }
        },

        swiper: {
            simulateTouch: {
                type: Boolean,
                default: true
            },

            slidesPerColumn: {
                type: Number,
                required: true
            },

            slidesPerGroup: {
                type: Number,
                required: false,
                default: null
            },

            slidesPerColumnFill: {
                type: String,
                default: 'column'
            }
        },

        title: {
            type: String,
            required: true
        },

        componentModel: ComponentModel().schema
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class Class
{
    static async test()
    {
    }
}

schema.loadClass(Class);
let nameModel = "ThemesSiteThemeLine";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}