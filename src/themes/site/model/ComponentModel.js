var mongoose = require('mongoose');
import {Conector} from "../../../database/mongodb/connector"

var schema = new mongoose.Schema(
    {
        searchTypeId: {
            type: Number,
            required: true
        },

        searchTypeDescription: {
            type: String,
            required: true
        },

        itens: {
            type: Array,
            required: false
        },

        data: {
            type: Object,
            required: false
        }
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class Class
{
    static async test()
    {
    }
}

schema.loadClass(Class);
let nameModel = "ThemesSiteComponent";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}