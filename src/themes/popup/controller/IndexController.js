var router = express.Router();

import PlanResourceMiddleware from '../../../middleware/planResourceMiddleware'

import ThemeModel from "../model/ThemeModel"


router.use(async (req, res, next) => {

    let resourceMidleware = new PlanResourceMiddleware(res, req, {
        resource: PlanResourceMiddleware.RESOURCE_POPUP
    })

    let middlewares = [resourceMidleware]

    for(let middleware of middlewares)
    {
       if(!await middleware.run())
        return await middleware.resFail()
    }

    return next()
})

router.route('/')
.post(function(req, res)
{
    let fnProcess = async () => {
        if(Object.keys(req.body.attributes).length > 0)
            return await ThemeModel().find(req.body.filters, req.body.attributes)
        else
            return await ThemeModel().find(req.body.filters)
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})
.put(function(req, res)
{
    let fnProcess = async () => {
        return await new ThemeModel()(req.body).save()
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
});

router.route('/:EntityID')
.patch(function(req, res)
{
    let fnProcess = async () => {
        let entityID = req.params.EntityID

        var mongoose = require('mongoose');
        entityID = mongoose.Types.ObjectId(entityID);

        return await ThemeModel().updateOne({_id: entityID}, req.body);
    }

    fnProcess().then(response => {
        if(response != null)
            ResponseHelper.setSuccess(res, 'ok')
        else
            ResponseHelper.setError(res, 'Object not found')
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})
.delete(function(req, res)
{
    let fnProcess = async () => {
        let entityID = req.params.EntityID
        return await ThemeModel().findOneAndRemove({_id: entityID});
    }
    
    fnProcess().then(response => {

        if(response == null)
            ResponseHelper.setError(res, 'Object not found')
        else
            ResponseHelper.setSuccess(res, 'OK')
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})

module.exports = router;