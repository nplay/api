var mongoose = require('mongoose');
var schema = new mongoose.Schema(
    {
        enabled: {
            type: Boolean,
            default: false,
            required: false
        },

        action: {
            type: String,
            required: true,
            default: null
        },

        actionName: {
            type: String,
            required: true,
        },

        page: {
            type: String,
            required: true,
            default: 'all'
        },

        typeName: {
            type: String,
            required: true
        },

        type: {
            type: String,
            required: true,
            default: null
        },

        allowDrop: {
            type: Boolean,
            default: false,
        },

        enabled: {
            type: Boolean,
            default: true,
        },

        additionalData: {
            type: Object,
            required: false,
            default: new Object
        },

        name: {
            type: String,
            required: true
        },

        html: {
            type: String,
            required: true
        }
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class Class
{
    static async test()
    {
    }
}

schema.loadClass(Class);
let nameModel = "ThemesPopupTheme";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}
