var router = express.Router();

import PlanResourceMiddleware from '../../../middleware/planResourceMiddleware'

import MenuHelper from '../helper/MenuHelper'
import CategoryModel from '../../../category/model/CategoryModel'
import MenuModel from "../model/MenuModel"
import DefaultThemesHelper from '../helper/DefaultMenuHelper'

router.use(async (req, res, next) => {

    let resourceMidleware = new PlanResourceMiddleware(res, req, {
        resource: PlanResourceMiddleware.RESOURCE_CATEGORY
    })

    let middlewares = [resourceMidleware]

    for(let middleware of middlewares)
    {
       if(!await middleware.run())
        return await middleware.resFail()
    }

    return next()
})

router.route('/:menuID?')
.get(function(req, res)
{
    let fnProcess = async () => {

        let methods = {
            async getMenu(menuID)
            {
                let menuHelper = new MenuHelper()
                return await menuHelper.get(menuID)
            },

            async hasCategories()
            {
                let categories = await new CategoryModel().findOne({},{})
                return categories != null
            },

            async installDefaultMenus()
            {
                return await new DefaultThemesHelper().install()
            }
        }

        let menuID = req.params.menuID
        let response = await methods.getMenu(menuID)

        if(menuID == null && response.length == 0 && await methods.hasCategories())
        {
            await methods.installDefaultMenus()
            response = await methods.getMenu(menuID)
        }
            
        return response
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    },
    error => {
        console.log(error)
        ResponseHelper.setError(res, error.toString())
    })
})
.post(function(req, res)
{
    let fnProcess = async (menu) => {
        let objMenu = new MenuModel()(menu);
        return await objMenu.save()
    }

    fnProcess(req.body).then(response => {
        ResponseHelper.setSuccess(res, response)
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
});

router.route('/:menuID')
.put(function(req, res)
{
    let fnProcess = async () => {
        let menuID = req.params.menuID
        let menu = await MenuModel().findOne({_id: menuID});

        if(menu == null)
            return false

        menu = Object.assign(menu, req.body)
        await menu.save()

        return menu
    }

    fnProcess().then(response => {

        if(response == false)
            return ResponseHelper.setWarning(res, 'Objeto não encontrado')
        else
            ResponseHelper.setSuccess(res, response)
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})
.delete(function(req, res)
{
    let fnProcess = async () => {
        let menuID = req.params.menuID
        await MenuModel().findOneAndRemove({_id: menuID});

        return true
    }

    fnProcess().then(response => {

        if(response == false)
            return ResponseHelper.setWarning(res, 'Objeto não encontrado')
        else
            ResponseHelper.setSuccess(res, response)
    },
    error => {
        ResponseHelper.setError(res, error.toString())
    })
})

module.exports = router;