class Status
{
   static get NOT_SENDING(){
      return 'not-sending'
   }

   static get SENDING(){
      return 'sending'
   }

   static get ERROR(){
      return 'error'
   }

   static get SENDED(){
      return 'sended'
   }
}

export default Status