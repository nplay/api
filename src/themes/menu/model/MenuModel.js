import Status from './Status'
import MetricModel from './menu/MetricModel'

var mongoose = require('mongoose');

var schema = new mongoose.Schema(
    {
        name: String, //Name type search

        description: {
            type: String,
            default: null
        },

        isCurrent: {
            type: Boolean,
            required: true,
            default: false
        },

        lastCurrentWrite: {
            type: Date,
            required: true,
            default: Date.now
        },

        metrics: [MetricModel.schema],

        allowDrop: {
            type: Boolean,
            required: true,
            default: false
        },

        title: {
            type: String,
            required: true
        },

        status: {
            type: String,
            required: true,
            default: Status.NOT_SENDING
        },

        data: {
            type: Object,
            required: false,
            default: null
        }
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class Class
{
    static async test()
    {
    }
}

schema.loadClass(Class);
let nameModel = "ThemesMenu";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}