var mongoose = require('mongoose');
import SummaryModel from './metric/SummaryModel'

var schema = new mongoose.Schema(
   {
      //Type of metric: ex: sell, buy
      type: {
         type: String,
         required: true
      },
      summary: [SummaryModel.schema]
   }
);

schema.query.byId = function(ids){
   return this.find({id: {$in: ids}});
};

class Class
{
   static async test()
   {
   }
}

schema.loadClass(Class);
let nameModel = "ThemesMenuMetric";

export default mongoose.model(nameModel, schema)