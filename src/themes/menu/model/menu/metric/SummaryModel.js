var mongoose = require('mongoose');
var schema = new mongoose.Schema(
   {
      date: {
         type: Date,
         required: true,
         default: Date.now
      },

      total: {
         type: Number,
         default: 0
      }
   }
);

schema.query.byId = function(ids){
   return this.find({id: {$in: ids}});
};

class Class
{
   static async test()
   {
   }
}

schema.loadClass(Class);
let nameModel = "ThemesMenuMetricSummary";

export default mongoose.model(nameModel, schema)