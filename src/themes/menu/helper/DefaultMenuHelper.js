import CategoryTreeSortHelper from "../../../category/helper/CategoryTree/SortHelper";
import Status from "../model/Status";
import MenuModel from '../model/MenuModel'
import SortLevelError from "../../../category/helper/CategoryTree/Sort/Error/LevelError";

/**
 * INSTALL DEFAULT THEME's
 */
class DefaultThemesHelper
{
   constructor()
   {
      this.defaultThemes = [
         {
            title: "Modelo padrão",
            isCurrent: true,
            description: "Modelo atual da loja",
            sorts: [],
            data: null,
            status: Status.SENDED
         },
         {
         title: "Modelo 1",
         isCurrent: false,
         description: "Mais vendidos\nOrdem alfabética\nMais vistos",
         sorts: [
            { by: CategoryTreeSortHelper.SORT_BEST_SELLER, level: 1 },
            { by: CategoryTreeSortHelper.SORT_ALPHABETICAL, level: 2 },
            { by: CategoryTreeSortHelper.SORT_BEST_VIEWED, level: 3 }
         ],
         data: null,
         status: Status.NOT_SENDING
      },
      {
         title: "Modelo 2",
         isCurrent: false,
         description: "Mais vendidos\Mais vendidos\nOrdem alfabética",
         sorts: [
            { by: CategoryTreeSortHelper.SORT_BEST_SELLER, level: 1 },
            { by: CategoryTreeSortHelper.SORT_BEST_SELLER, level: 2 },
            { by: CategoryTreeSortHelper.SORT_ALPHABETICAL, level: 3 }
         ],
         data: null,
         status: Status.NOT_SENDING
      }]
   }

   async install()
   {
      for(let defaultTheme of this.defaultThemes)
      {
         let menu = await this._sortCategory(defaultTheme)
         let menuModel = new MenuModel()(defaultTheme)

         menuModel.data = menu

         await menuModel.save()
      }
   }

   async _sortCategory(theme)
   {
      let categoryHelper = new CategoryTreeSortHelper()

      try
      {
         for(let sort of theme.sorts)
            await categoryHelper.sortLevel(sort.by, sort.level)
      }
      catch (error)
      {
         if(!(error instanceof SortLevelError))
            throw error
      }
      
      return await categoryHelper.getTree()
   }
}

export default DefaultThemesHelper