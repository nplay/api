import MenuModel from "../model/MenuModel"
import Status from '../model/Status'

class MenuHelper
{
   constructor(connection)
   {
      this.connection = connection || null
   }

   static get STATUS()
   {
      return Status
   }

   async get(menuID)
   {
      let objFind = {}
      let methodInvoke = 'find'

      if(menuID != undefined)
      {
         Object.assign(objFind, {_id: menuID})
         methodInvoke = 'findOne'
      }

      let result = await MenuModel()[methodInvoke](objFind);
      return result
   }
}

export default MenuHelper