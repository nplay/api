import AbstractMetric from "./AbstractMetric";

class SellMetric extends AbstractMetric
{
   constructor(connection)
   {
      super('sell', connection);
   }
}

export default SellMetric