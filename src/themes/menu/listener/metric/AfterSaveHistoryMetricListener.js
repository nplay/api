
let Conector = require('../../../../database/mongodb/connector').Conector
import ViewMetric from './ViewMetric'

/**
 * Add metric Views after saved History Model (Microservice API)
 */
module.exports = async (historyModel, identifier) => {

    var conn = await Conector.connect(identifier)

    let viewMetric = new ViewMetric(conn)

    let curMenu = await viewMetric.currentMenuCategory()

    if (!curMenu)
        return false;

    let metricEntity = await viewMetric.getMetric(curMenu)

    if (metricEntity.metric == null)
        await viewMetric.createMetric(curMenu)
    else
        await viewMetric.addMetricTotal(metricEntity, curMenu)
}