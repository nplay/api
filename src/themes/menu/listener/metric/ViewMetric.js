import AbstractMetric from "./AbstractMetric";

class ViewMetric extends AbstractMetric
{
   constructor(connection)
   {
      super('view', connection);
   }
}

export default ViewMetric