
import DateHelper from '../../../../helper/DateHelper'
import MenuModel from '../../model/MenuModel'

class AbstractMetric
{
   constructor(type, connection)
   {
      this.connection = connection || null
      this.type = type
   }

   async currentMenuCategory(){
      return await MenuModel(this.connection).findOne({
         isCurrent: true
      })
   }

   diffDays(startDate, endDate)
   {
      return new DateHelper().diff(startDate, endDate)
   }

   async getMetric(curMenu)
   {
      let curDate = new DateHelper().toTimezone().toISOString()
      let lastDayTime = new DateHelper().subtract(1, curDate).getTime()

      let lastWriteDataTime = new Date(curMenu.lastCurrentWrite).getTime()

      let response = {
         metric: null,
         summary: null
      }

      for(let metric of curMenu.metrics)
      {
         if(metric.type != this.type)
            continue;

         response.metric = metric

         for(let summary of metric.summary)
         {
            let dateTime = new Date(summary.date).getTime()
            let gtLastWrite = dateTime >= lastWriteDataTime
            let gtLastDay = dateTime >= lastDayTime

            if(!gtLastWrite || !gtLastDay)
               continue
            
            response.summary = summary
            break
         }

         if(!response.summary)
         {
            let summary = {
               date: curDate,
               total: 1
            }

            metric.summary.push(summary)
            response.summary = summary
         }

         if(response.metric)
            break;
      }

      return response
   }

   async createMetric(curMenu)
   {
      let metricModel = {
         type: this.type,
         summary: [{
            date: new DateHelper().toTimezone().toISOString(),
            total: 1
         }]
      }

      curMenu.metrics.push(metricModel)

      await MenuModel(this.connection).updateOne({
         '_id': curMenu._id },
      {
         $set: {
            'metrics': curMenu.metrics
         }
      })
   }

   async addMetricTotal(metricEntity, curMenu)
   {
      metricEntity.summary.total += 1
      curMenu.markModified('metrics')

      await curMenu.save()
   }
}

export default AbstractMetric