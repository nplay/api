import StorageHelper from "../../../storage/helper/StorageHelper";

class AbstractSearchesAttributeHelper
{
   constructor(limit)
   {
      this.limit = limit || 15
      this.storageHelper = new StorageHelper()
   }

   /**
    * @return Array attributes
    */
   async getAttributes(ids, attributes)
   {
      let query = {
         id: { $in: ids }
      }

      let projection = new Object()

      for(let attribute of attributes)
      {
         query[attribute] = { $exists: true }

         if(!projection.hasOwnProperty(attribute))
            projection[attribute] = 1
      }

      let response = await this.storageHelper.findOne(query, projection).lean()
      return response
   }

   /**
    * @param find(query) filter 
    */
   async getIds(query)
   {
      let projection = { id: 1 }
      let finalQuery = {
         allowed: true
      }

      for(let key of Object.keys(query))
      {
         let queryValue = query[key]
         
         if(!finalQuery.hasOwnProperty(key))
            finalQuery[key] = { $in : []}

         if(Array.isArray(queryValue))
            finalQuery[key]['$in'] = finalQuery[key]['$in'].concat(queryValue)
         else
            finalQuery[key]['$in'].push(queryValue)
      }

      let responseCollection = await this.storageHelper.find(finalQuery, projection).lean()

      return responseCollection.map(response => {
         return response.id
      })
   }

   async byAttribute(attributes, ids)
   {
      //1St
      let response = await this.getAttributes(ids, attributes)
      
      if(!response)
         return null

      let attribResponse = new Object(response)

      if(attribResponse.hasOwnProperty('_id'))
         delete attribResponse['_id']

      //2St
      let idsResponse = await this.getIds(attribResponse)
      return idsResponse
      
      //3St child class implement...
   }
}

export default AbstractSearchesAttributeHelper