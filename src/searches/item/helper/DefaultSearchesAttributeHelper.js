import AbstractSearchesAttributeHelper from "./AbstractSearchesAttributeHelper";

class DefaultSearchesAttributeHelper extends AbstractSearchesAttributeHelper
{
   constructor(limit)
   {
      super(limit)
   }

   async byAttribute(attributes, ids)
   {
     let itensID = await super.byAttribute(attributes, ids)

     //Retrive final itens data
     let query = { id: {$in: itensID} }

     return this.find(query)
   }

   async byCategorys(categorys)
   {
      let query = {
         categorys: { $in: categorys }
      }
      
      return this.find(query)
   }

   async find(query)
   {
      query = Object.assign({
         allowed: true
      }, query)

      return this.storageHelper.find(query, {__v: 0, _id: 0}).limit(this.limit)
   }
}

export default DefaultSearchesAttributeHelper