import BuyToBuyModel from '../../mining/buyToBuy/model/buyToBuyModel'
import ItemModel from "../../storage/model/ItemModel"

module.exports = (router) => {
    router.route('/')
    .post(function(req, res)
    {
        let fnProcess = async () => {
            let limit = parseInt(req.query.limit || 16)
            let itens = req.body.ecommerce.impressions
    
            let itemIds = itens.map(item => {
                return item.id
            })
    
            let miningItens = await BuyToBuyModel().find({
                from: {$in: itemIds}
            }, {to: 1, _id: 0}).limit(limit).lean()
    
            let miningItensID = new Array()
    
            for(let miningItem of miningItens)
            {
                for(let itemTo of miningItem.to)
                {
                    if(miningItensID.indexOf(itemTo) == -1)
                        miningItensID.push(itemTo)
                }
            }
    
            let responseItens = await ItemModel().find(
                {
                    id: { $in: miningItensID },
                    allowed: true
                },
                {_id: false, __v: false}
            ).lean()
    
            return responseItens
        }
        
        fnProcess().then(response => {
            ResponseHelper.setSuccess(res, response)
        })
    });
    
    return router
}
