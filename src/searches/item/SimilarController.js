
import SimilarModel from '../../mining/similar/model/similarModel'
import ItemModel from '../../storage/model/ItemModel'

module.exports = (router) => {
    router.route('/')
    .post(function(req, res)
    {
        let fnProcess = async () => {
            let limit = parseInt(req.query.limit || 16)
    
            let productIds = req.body.ecommerce.impressions.map( impression => impression.id )

            let itens = await SimilarModel().find({
                id: {$in: productIds}
            }, {
                ids: 1,
                _id: 0
            }).lean()

            let ids = new Array
            let limitReal = limit / productIds.length
            let limitByItem = Math.floor(limitReal)

            for(let idx = 0; idx < itens.length; ++idx)
            {
                let item = itens[idx]
                let isFinal = idx == productIds.length -1
                let remainingItens = limitByItem
                
                for(let id of item.ids)
                {
                    let hasItem = ids.indexOf(id) > -1
                    
                    if(hasItem)
                        continue;
                    
                    if(remainingItens-- > 0)
                        ids.push(id)
                    else if(isFinal && limitReal % 1 > 0)
                    {
                        ids.push(id)
                        break
                    }
                }
            }
    
            let itemCollection = await ItemModel().find({
                id: {
                    $in: ids
                },
                allowed: true
            }, {__v: 0, _id: 0, description: 0, categorys: 0})
    
            return itemCollection
        }
        
        fnProcess().then(response => {
            ResponseHelper.setSuccess(res, response)
        })
    });

    return router
}