
import DefaultSearchesAttributeHelper from "../helper/DefaultSearchesAttributeHelper"

module.exports = (router) => {
    router.post('/category', function (req, res) {
        let limit = parseInt(req.query.limit || 16)
        let attributeHelper = new DefaultSearchesAttributeHelper(limit)

        let ids = req.body.ids

        let fnProcess = async () => {
            return await attributeHelper.byCategorys(ids)
        }

        fnProcess().then(itens => {
            ResponseHelper.setSuccess(res, itens)
        })
    });

    return router
}