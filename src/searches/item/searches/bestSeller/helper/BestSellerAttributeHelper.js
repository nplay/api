import AbstractSearchesAttributeHelper from "../../../helper/AbstractSearchesAttributeHelper";
import AnalyticsOrderModel from "../../../../../analytics/model/OrderModel"

class BestSellerAttributeHelper extends AbstractSearchesAttributeHelper {
   constructor(limit) {
      super(limit)
   }

   async byAttribute(attributes, ids) {
      let itensID = await super.byAttribute(attributes, ids)

      if(!itensID)
         return null

      let orderItensID = await this._orderItens(itensID)
      orderItensID = orderItensID.map(item => {return item.id})

      //Retrive final itens data
      let query = { allowed: true, id: { $in: orderItensID } }

      return this.storageHelper.find(query, {__v: 0, _id: 0})
   }

   async byCategorys(categorys){
      let query = { allowed: true, categorys: { $in: categorys } }
      let storageItens = await this.storageHelper.find(query, {id: 1, _id: 0}).lean()

      if(storageItens.length == 0)
         return null

      let itensID = storageItens.map( item => {return item.id})

      itensID = await this._orderItens(itensID)
      itensID = itensID.map(item => {return item.id})
      query = { allowed: true, id: { $in: itensID } }

      return this.storageHelper.find(query, {__v: 0, _id: 0})
   }

   async all()
   {
      let orderItens = await this._orderItens([])
      let itensID = orderItens.map(item => {return item.id})

      let query = { allowed: true, id:  {$in: itensID } }
      return await this.storageHelper.find(query, {__v: 0, _id: 0})
   }

   async _orderItens(itensID) {

      let query = [
         {
            $unwind: "$items"
         },
      ]

      if(itensID.length > 0)
      {
         query.push({
            $match: {
               "items.id": {
                  $in: itensID
               }
            },
         })
      }

      query = query.concat([
         
         {
            $group: {
               _id: "$items.entityId",
               qty: {
                  $sum: '$items.quantity'
               },
               item: {
                  $first: "$items"
               }
            }
         },
         
         {
            $sort: {
               qty: -1
            }
         },

         {
            $limit: this.limit
         },
         
         {
            $project: {
               _id: 0,
               id: "$item.id"
            }
         }
      ])

      return await AnalyticsOrderModel().aggregate(query)
   }
}

export default BestSellerAttributeHelper