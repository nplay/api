import BestSellerAttributeHelper from "../helper/BestSellerAttributeHelper";

module.exports = (router) => {
	router.route('/')
		.get(function (req, res) {
			let fnProcess = async () => {
				let limit = parseInt(req.query.limit || 16)
				let helper = new BestSellerAttributeHelper(limit)
				return await helper.all()
			}

			fnProcess().then(itens => {
				ResponseHelper.setSuccess(res, itens)
			})
		});

	router.post('/attribute', function (req, res) {
		let fnProcess = async () => {
			let ids = req.body.ids
			let attributes = req.body.attributes

			let limit = parseInt(req.query.limit || 16)
			let helper = new BestSellerAttributeHelper(limit)
			return await helper.byAttribute(attributes, ids)
		}

		fnProcess().then(itens => {
			ResponseHelper.setSuccess(res, itens || new Array)
		})
	});

	router.route('/category/:categoryId/')
		.get(function (req, res) {
			let fnProcess = async () => {
				let categoryID = parseInt(req.params.categoryId)

				let limit = parseInt(req.query.limit || 16)
				let helper = new BestSellerAttributeHelper(limit)
				return await helper.byCategorys([categoryID])
			}

			fnProcess().then(itens => {
				ResponseHelper.setSuccess(res, itens || new Array)
			})
		});

	router.route('/categorys/')
		.post(function (req, res) {
			let fnProcess = async () => {
				let categorys = req.body.ids

				let limit = parseInt(req.query.limit || 16)
				let helper = new BestSellerAttributeHelper(limit)
				return await helper.byCategorys(categorys)
			}

			fnProcess().then(itens => {
				ResponseHelper.setSuccess(res, itens || new Array)
			})
		});

	return router
}