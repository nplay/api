import AbstractSearchesAttributeHelper from "../../../helper/AbstractSearchesAttributeHelper";
import HistoryModel from "../../../../../history/model/HistoryModel"
import { SSOOIDC } from "aws-sdk";

class BestViewedAttributeHelper extends AbstractSearchesAttributeHelper {
   constructor(limit) {
      super(limit)
   }

   async byAttribute(attributes, ids){
      let itensID = await super.byAttribute(attributes, ids)

      if(!itensID)
         return null

      //Retrive final itens data
      let itens = await this.viewedItens(itensID)
      let query = { allowed: true, id: {$in: itens.map(item => { return item.id })} }

      return await this.storageHelper.find(query)
   }

   async byCategorys(categorys){
      let query = { categorys: { $in: categorys } }

      let storageItens = await this.storageHelper.find(query, {id: 1, _id: 0}).lean()
     
      if(storageItens.length == 0)
         return []

      let itensID = storageItens.map( item => {return item.id})

      itensID = await this.viewedItens(itensID)
      itensID = itensID.map(item => { return item.id })

      query = { allowed: true, id: { $in: itensID } }
      return this.storageHelper.find(query, {__v: 0, _id: 0})
   }

   async all()
   {
      let itensID = await this.viewedItens([])
      itensID = itensID.map(item => { return item.id })
      
      let query = { allowed: true, id: { $in: itensID } }
      
      return this.storageHelper.find(query, {__v: 0, _id: 0})
   }

   async viewedItens(ids)
   {
      let query = [
         {
            $unwind: "$itens"
         }
      ]

      if(ids.length > 0)
      {
         query.push(
         {
            $match: {
               'itens.product.id': {
                  $in: ids
               }
            }
         })
      }

      query = query.concat([
         {
            $group: {
               _id: "$itens.product.id",
               total: {
                  $sum: "$itens.views"
              }
            }
         },
         {
            $sort: {
               total: -1
            }
         },
         {
            $limit: this.limit
         },
         {
            $project: {
               _id: 0,
               id: '$_id'
            }
        }
      ])

      return await HistoryModel().aggregate(query)
   }
}

export default BestViewedAttributeHelper