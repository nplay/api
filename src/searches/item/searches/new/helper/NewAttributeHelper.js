import AbstractSearchesAttributeHelper from "../../../helper/AbstractSearchesAttributeHelper";

class NewAttributeHelper extends AbstractSearchesAttributeHelper
{
   constructor(limit)
   {
      super(limit)
   }

   async byAttribute(attributes, ids)
   {
     let itensID = await super.byAttribute(attributes, ids)

     //Retrive final itens data
     let query = { id: {$in: itensID} }

     return this.find(query)
   }

   async find(query)
   {
      return this.storageHelper.find(this.prepareQuery(query), {__v: 0, _id: 0}).limit(this.limit)
   }

   prepareQuery(query)
   {
      query = query || {}

      Object.assign(query, {
         hasNew: true,
         allowed: true
      })

      return query
   }

   getLastMonth()
   {
      let lastMonth = new Date()
      lastMonth.setDate(lastMonth.getDate()-30)
      return lastMonth
   }
}

export default NewAttributeHelper