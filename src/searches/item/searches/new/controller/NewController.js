
import NewAttributeHelper from "../helper/NewAttributeHelper";

module.exports = (router) => {
    router.route('/category/:categoryId?/')
        .get(function (req, res) {
            let limit = parseInt(req.query.limit || 16)
            let newHelper = new NewAttributeHelper(limit)

            let fnProcess = async () => {
                let categoryID = parseInt(req.params.categoryId)
                let attributes = {}

                if (null != categoryID)
                    attributes.categorys = { $in: [categoryID] }

                return await newHelper.find(attributes)
            }

            fnProcess().then(itens => {
                ResponseHelper.setSuccess(res, itens)
            })
        });

    router.route('/')
        .get(function (req, res) {
            let fnProcess = async () => {
                let limit = parseInt(req.query.limit || 16)
                return new NewAttributeHelper(limit).find({})
            }

            fnProcess().then(itens => {
                ResponseHelper.setSuccess(res, itens)
            })
        });

    router.post('/attribute', function (req, res) {
        let limit = parseInt(req.query.limit || 16)
        let newHelper = new NewAttributeHelper(limit)

        let attributes = req.body.attributes
        let ids = req.body.ids

        let fnProcess = async () => {
            return await newHelper.byAttribute(attributes, ids)
        }

        fnProcess().then(itens => {
            ResponseHelper.setSuccess(res, itens)
        })
    });

    router.route('/categorys')
        .post(function (req, res) {
            let limit = parseInt(req.query.limit || 16)
            let newHelper = new NewAttributeHelper(limit)

            let fnProcess = async () => {
                let categorys = req.body.ids
                let attributes = {}

                attributes.categorys = { $in: categorys }

                return await newHelper.find(attributes)
            }

            fnProcess().then(itens => {
                ResponseHelper.setSuccess(res, itens)
            })
        });

    return router
}