import AbstractSearchesAttributeHelper from "../../../helper/AbstractSearchesAttributeHelper";

class PromotionalAttributeHelper extends AbstractSearchesAttributeHelper {
   constructor(limit) {
      super(limit)
   }

   async byAttribute(attributes, ids) {
      let itensID = await super.byAttribute(attributes, ids)

      //Retrive final itens data
      let query = {
         sale_price: { $exists: true },
         id: {
            $in: itensID
         }
      }

      return this.find(query)
   }

   async byCategorys(categorys)
   {
      let query = {
         categorys: { $in: categorys },
         sale_price: { $exists: true }
      }
      
      return this.find(query)
   }

   async all()
   {
      return this.find({sale_price: { $exists: true }})
   }

   async find(query)
   {
      query = Object.assign({
         allowed: true
      }, query)
      
      return this.storageHelper.find(query, {__v: false, _id: false}).limit(this.limit)
   }
}

export default PromotionalAttributeHelper