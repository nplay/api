
import AnalyticsOrderModel from "../../../analytics/model/OrderModel"
import ItemModel from "../../../storage/model/ItemModel"

class BestSellectHelperClass
{
    constructor()
    {
        this.categoryTree = null
    }

    async bestSellerCategorys()
    {
        let storageItensID = await ItemModel().distinct('id')
        
        let itens = await AnalyticsOrderModel().bestSeller(storageItensID)
        let categorys = new Object;

        for(let item of itens)
        {
            for(let categoryID of item.categorys)
            {
                if(categorys.hasOwnProperty(categoryID) == false)
                    categorys[categoryID] = 0

                categorys[categoryID] += item.quantity
            }
        }

        return categorys
    }
}

module.exports = {
    BestSellerHelper: BestSellectHelperClass
}