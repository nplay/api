
import HistoryModel from "../../../history/model/HistoryModel"
import ItemModel from "../../../storage/model/ItemModel"

class BestViewedClass
{
    async bestViewed()
    {
        let storageItensID = await ItemModel().distinct('id')
        return await HistoryModel().bestViewed(null, storageItensID)
    }
}

module.exports = {
    BestViewedHelper: BestViewedClass
}