
import CategoryModel from "../../category/model/CategoryModel"
import { BestSellerHelper } from "./bestseller/Helper"
import { CategoryTreeHelper } from "../../category/helper/CategoryTreeHelper"

import { BestViewedHelper } from "../category/bestviewed/Helper"

module.exports = (router) => {
	router.get('/', function (req, res) {
		let fnProcess = async () => {
			let categories = await CategoryModel().find({}, { _id: false, __v: false })
			return CategoryModel().getTree(categories)
		}
	
		fnProcess().then(response => {
			ResponseHelper.setSuccess(res, response)
		})
	})
	.get('/best-viewed', function (req, res) {
		let fnProcess = async () => {
			let categoryTreeHelper = await new CategoryTreeHelper()._mount();
			let bestViewed = await new BestViewedHelper().bestViewed()
			categoryTreeHelper.assignAttributes({ views: 0 })
	
			for (let key of Object.keys(bestViewed))
			{
				let ocurrences = bestViewed[key]
				let categoryID = key
				let node = categoryTreeHelper.searchByAttribute('id', categoryID)
				
				if(!node)
					continue;
				
				node.views += ocurrences
			}
	
			categoryTreeHelper.sortByAttribute('views')
			return categoryTreeHelper._tree;
		}
	
		fnProcess().then(response => {
			ResponseHelper.setSuccess(res, response)
		},
			error => {
				throw error
			})
	})
	.get('/best-seller', function (req, res) {
	
		let fnProcess = async () => {
			let categoryTreeHelper = await new CategoryTreeHelper()._mount()
			let bestCategorys = await new BestSellerHelper().bestSellerCategorys()
	
			categoryTreeHelper.assignAttributes({ sells: 0 })
	
			for (let key of Object.keys(bestCategorys))
			{
				let categoryID = key
				let sells = bestCategorys[key]
	
				let node = categoryTreeHelper.searchByAttribute('id', categoryID)
				
				if(node == null)
					continue;
				
				node.sells += sells
			}
	
			categoryTreeHelper.sortByAttribute('sells')
			return categoryTreeHelper._tree;
		}
	
		fnProcess().then(response => {
			ResponseHelper.setSuccess(res, response)
		})
	});

	return router
}