import AbstractHistoryAttributeHelper from "../../helper/AbstractHistoryAttributeHelper";
import HistoryModel from "../../../../history/model/HistoryModel"

class HistoryAttributeHelper extends AbstractHistoryAttributeHelper {
   constructor(limit) {
      super(limit)
   }

   async byAttribute(attributes, ids, sessionID) {
      let itensID = await super.byAttribute(attributes, ids)

      return this._itens(itensID, sessionID)
   }

   async byCategorys(categorys, sessionID)
   {
      let result = await this.storageHelper.find({allowed: true, categorys: { $in: categorys }}, {id: 1, _id: 0})
      let itensID = result.map(item => { return item.id })

      if(itensID == null)
         return []

      return this._itens(itensID, sessionID)
   }

   async all(sessionID)
   {
      let historyItens = await this._itens(null, sessionID)

      let storageItens = await this.storageHelper.find({
         entityId: {
            '$in': historyItens.map((item) => item.entityId)
         },
         allowed: true
      }, {id: 1, _id: 0})

      let itensID = storageItens.map(item => { return item.id })

      if(itensID == null)
         return []

      return await this._itens(itensID, sessionID)
   }

   async _itens(itensID = null, sessionID)
   {
      let match = {
         "sessionID": sessionID
      }

      let query = [{
         $unwind: "$itens"
      },
      {
         $match: match
      },
      {
         $limit: this.limit
      },
      {
         $group: {
            product: {
               $first: "$itens.product"
            },
            _id: '$itens.product.id'
         }
      },
      {
         $replaceRoot: {
            newRoot: '$product'
         }
      }]

      if(itensID)
      {
         match["itens.product.id"] = {
            $in: itensID
         }
      }

      return await HistoryModel().aggregate(query)
   }
}

export default HistoryAttributeHelper