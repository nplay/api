
import HistoryAttributeHelper from "../helper/HistoryAttributeHelper";

module.exports = (router) => {

    router.post('/', function(req, res) {
        let fnProcess = async (sessionId) => {
            let limit = parseInt(req.query.limit || 16)
            return await new HistoryAttributeHelper(limit).all(sessionId)
        }
    
        let sessionId = req.body.sessionID
    
        if(null == sessionId)
            return ResponseHelper.setWarning(res, 'Session ID não encontrado');
    
        fnProcess(sessionId)
        .then(result => {
            ResponseHelper.setSuccess(res, result)
        }, error => {
            ResponseHelper.setError(res, error.message)
        })
    });
    
    router.post('/attribute', function (req, res) {
        let fnProcess = async () => {
            let limit = parseInt(req.query.limit || 16)
            let ids = req.body.ids
            let attributes = req.body.attributes
            let sessionID = req.body.sessionID
    
            let helper = new HistoryAttributeHelper(limit)
            return await helper.byAttribute(attributes, ids, sessionID)
        }
    
        fnProcess().then(result => {
            ResponseHelper.setSuccess(res, result)
        })
    });
    
    router.post('/category/:categoryID', function(req, res) {
        
        let fnProcess = async (sessionId) =>
        {
            let limit = parseInt(req.query.limit || 16)
            let categoryID = parseInt(req.params.categoryID)

            let helper = new HistoryAttributeHelper(limit)
            return await helper.byCategorys([categoryID], sessionId)
        }
    
        let sessionId = req.body.sessionID
    
        if(null == sessionId)
            return ResponseHelper.setWarning(res, 'Session ID não encontrado');
    
        fnProcess(sessionId)
        .then(result => {
            ResponseHelper.setSuccess(res, result)
        }, error => {
            ResponseHelper.setError(res, error.message)
        })
    });

    return router
}