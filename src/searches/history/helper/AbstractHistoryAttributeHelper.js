import AbstractSearchesAttributeHelper from "../../item/helper/AbstractSearchesAttributeHelper"

class AbstractHistoryAttributeHelper extends AbstractSearchesAttributeHelper
{
   constructor(limit)
   {
      super(limit)
   }
}

export default AbstractHistoryAttributeHelper