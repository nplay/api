
import TokenGenerator from './token/generator';
import TokenAutenticator from './token/autenticator';

class PermissionHelper
{
    constructor()
    {
        this.tokenGenerator = new TokenGenerator
        this.tokenAutenticator = new TokenAutenticator
    }
}

export default PermissionHelper