
import PayloadModel from '../../model/key/payloadModel'

class TokenGenerator
{
    genPrivate()
    {
        return new Date().getTime() + Math.random()
    }

    _from(domains, platform, dId, scopes)
    {
        let payload = Object.create(PayloadModel)

        payload.domains = domains.join(',')
        payload.scope = scopes
        payload.platform = platform
        payload.dId = dId

        return payload
    }

    /**
     * 
     * @param {String} privKey 
     * @param {Array} domains
     * @param {String} scopes ex: user.path,user.get
     */
    genPublic(privKey = this.private(), domains, platform, dId, scopes)
    {
        let payload = this._from(domains, platform, dId, scopes)
        let jwt = require('jsonwebtoken');
        
        return {
            privateKey: privKey,
            publicKey: jwt.sign(Object.assign({}, payload), privKey),
            payload: payload
        }
    }
}

export default TokenGenerator