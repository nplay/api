
export default [
    {
        path: '/user/:database?',
        method: 'PATCH',
        scope: `user.path`
    }
]