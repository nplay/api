
import routes from './autenticator/routes'
import {UserModel} from '../../../user/model/UserModel'
import PayloadModel from '../../model/key/payloadModel'
import {Config} from "core"
const pathToRegexp = require('path-to-regexp')

class TokenAutenticator
{
    /**
     * @param {Array} publicKeys
     */
    async autenticate(publicKeys, req)
    {
        let coreConn = Conector.connectCore()

        let user = await UserModel(coreConn).byPublicKeys(publicKeys)

        if(!user)
            return false

        let allowed = await this._verify(req, user)
        return !allowed ? null : user
    }

    payload(privateKey, publicKey)
    {

        try {
            let jwt = require("jsonwebtoken")
            let decoded = jwt.verify(publicKey, privateKey);

            let payloadModel = Object.assign(PayloadModel, decoded)
            return payloadModel
        } catch (error) {
            return false
        }
    }

    async _verify(req, user)
    {
        let authorizationToken = req.header('Authorization-token')
        let payload = this.payload(user.privateKey, authorizationToken)

        if(await this._verifyWhiteDomain(req, payload))
            return true
        
        return await this._verifyScope(req, user, payload)
    }

    async _verifyScope(req, user, payload)
    {
        let currentPath = req.path
        let currentMethod = req.method

        let allowed = true

        for(let route of routes)
        {
            if(route.method != currentMethod)
                continue

            let regexp = pathToRegexp(route.path)
            let match = regexp.exec(currentPath)

            if(!match)
                continue

            let payloadScopes = payload.scope.split(',')
            let routeScopes = route.scope.split(',')

            let blocked = payloadScopes.some(payloadScope => {return routeScopes.indexOf(payloadScope) == -1})
            
            if(blocked)
                throw new Error(`You scopes: '${payloadScopes}' required: '${routeScopes}'`)

            allowed = true
            break
        }

        if(!allowed)
            throw new Error(`Route not mapped with scope`)

        return allowed
    }

    async _verifyWhiteDomain(req, payload)
    {
        let origin = req.header('origin') || ""
        let whiteDomains = Config.whiteDomainsXp

        let result = origin.match(whiteDomains)
        return result != null
    }
}

export default TokenAutenticator