import AbstractMiddleware from "./abstractMiddleware";
import RequestModel from '../request/model/requestModel'
import DateHelper from "../helper/DateHelper";

import UserHelper from '../helper/UserHelper'

class PlanRequestMiddleware extends AbstractMiddleware
{
   constructor(res, req, additionalData)
   {
      super(res, req, additionalData)
   }

   get message()
   {
      return 'Max recommendations in month'
   }

   async resFail()
   {
      this.res.status(403).json({message: this.message, code: 3});
   }

   async run()
   {
      let inLimit = await this._maxRequests()
      return inLimit == false
   }

   async _maxRequests()
   {
      let dateHelp = new DateHelper
      
      let curDate = dateHelp.toTimezone().toISOString()
      let lastDate = dateHelp.subtract(30, curDate)

      let requestModel = await RequestModel().find({
         created_at: {
            $gte: new Date(lastDate),
            $lte: new Date(curDate)
        }
      },
      {
         requests: 1
      })

      if(requestModel.length == 0)
         return false

      let total = requestModel.reduce((node, next) => {
         node.requests += next.requests
         return node
      })

      let userHelper = new UserHelper()
      let entity = await userHelper.storeByDB()

      return entity.store.plan.requestsLimit <= total.requests
   }
}

export default PlanRequestMiddleware