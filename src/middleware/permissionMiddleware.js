
import AbstractMiddleware from './abstractMiddleware'
import PermissionHelper from '../permission/helper/permissionHelper';
const pathToRegexp = require('path-to-regexp')
var httpContext = require('express-http-context');
import UserHelper from '../helper/UserHelper'

class PermissionMiddleware extends AbstractMiddleware
{
    constructor(res, req)
    {
        super(res, req)

        this.message = 'Forbidden'

        this.token = req.header('Authorization-token');
        this._conector = Conector;

        this.permissionHelper = new PermissionHelper

        this._publicRoutes = [
            {path: '/user/:token?', method: 'GET'},
            {path: '/user/login', method: 'POST'},
            {path: '/user/', method: 'POST'},
            {path: '/ti/ping', method: 'GET'},
            {path: '/ti/git-pull', method: 'GET'}
        ]
    }

    async run()
    {
        try {
            return await this._checkPermission()
        } catch (error) {
            this.message = error.message
            return false
        }
    }

    isPulicPath()
    {
        var me = this

        let authorized = this._publicRoutes.some( route => {
            let method = route.method
            let path = route.path

            if(method != me.req.method)
                return false

            let regexp = pathToRegexp(path)
            let match = regexp.exec(me.req.path)
            return match != null
        })

        return authorized
    }

    async _checkPermission()
    {
        if(this.isPulicPath())
            return true

        let user = await this.permissionHelper.tokenAutenticator.autenticate([this.token], this.req)

        if(user)
        {
            let entity = await new UserHelper().storeByPublicKeys([this.token])
            httpContext.set('store', entity.store)
            return true;
        }

        return false
    }

    async resFail()
    {
       this.res.status(403).json({message: this.message, code: 2});
    }
}

export default PermissionMiddleware