
class AbstractMiddleware
{
   constructor(responseInstance, requestInstance, additionalData)
   {
      this.additionalData = additionalData
      this.res = responseInstance
      this.req = requestInstance
   }

   async run()
   {
      throw new Error('Please extends method in child class')
   }

   async resFail()
   {
      throw new Error('Please extends method in child class')
   }
}

export default AbstractMiddleware