import AbstractMiddleware from "./abstractMiddleware";
import RequestModel from '../request/model/requestModel'
import DateHelper from "../helper/DateHelper";

import UserHelper from '../helper/UserHelper'

class PlanResourceMiddleware extends AbstractMiddleware
{
   constructor(res, req, additionalData)
   {
      super(res, req, additionalData)
   }

   static get RESOURCE_CATEGORY()
   {
      return 'category'
   }

   static get RESOURCE_VITRINE()
   {
      return 'vitrine'
   }

   static get RESOURCE_POPUP()
   {
      return 'popup'
   }

   get message()
   {
      return `Resource ${this.additionalData.resource} not allowed in plan`
   }

   async resFail()
   {
      this.res.status(403).json({message: this.message, code: 1});
   }

   async run()
   {
      return await this._check()
   }

   async _check()
   {
      let userHelper = new UserHelper()
      let entity = await userHelper.storeByDB()

      return entity.store.plan.resources[this.additionalData.resource] == true
   }
}

export default PlanResourceMiddleware