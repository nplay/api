import AbstractMiddleware from "./abstractMiddleware";
import RequestModel from '../request/model/requestModel'
import DateHelper from "../helper/DateHelper";
import {Config} from "core"

class RequestMiddleware extends AbstractMiddleware
{
   constructor(res, req, additionalData)
   {
      super(res, req, additionalData)

      this.config = Config
      this.dateHelp = new DateHelper();
   }

   static get ONEDAY()
   {
      return 1
   }

   async resFail()
   {
      throw new Error('Fail persist requested route')
   }

   async isAdmin()
   {
      return this.database == this.config.database.token
   }

   get database()
   {
      return Conector.getDB()
   }

   async run()
   {
      if(!this.database || await this.isAdmin())
         return true

      let path = `${this.req.baseUrl}`
      let method = this.req.method

      let curDate = this.dateHelp.toTimezone().toISOString()
      let lastDate = this.dateHelp.subtract(1, curDate)

      let conn = await Conector.connect(this.database)

      let requestModel = await RequestModel(conn).findOne({
         path: path,
         method: method,
         created_at: {
            $gte: new Date(lastDate),
            $lte: new Date(curDate)
        }
      })

      if(requestModel == null)
      {
         await new RequestModel(conn)({
            path: path,
            method: method,
            created_at: new Date(curDate)
         }).save()

         return true
      }

      requestModel.requests += 1
      await RequestModel(conn).updateOne({_id: requestModel._id}, {
         requests: requestModel.requests
      })

      return true
   }
}

export default RequestMiddleware