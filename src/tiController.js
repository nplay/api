
var router = express.Router();

const { exec, spawn } = require('child_process');

router.get('/ping', function(req, res) {
    ResponseHelper.setSuccess(res, 'ok')
});

module.exports = router;