var mongoose = require('mongoose');

let Conector = require('../../database/mongodb/connector').Conector

import ItemModel from "./HistoryItemModel"
import ProspectItemModel from "./HistoryProspectItemModel"
import HistoryUserAgentModel from './HistoryUserAgent'
import HistoryWeatherModel from './HistoryWeather'
import HistoryGeolocationModel from "./HistoryGeolocationModel";

var schema = new mongoose.Schema(
    {
        created_at: Date,
        itens: [ItemModel().schema],
        prospectItens: [ProspectItemModel().schema],
        userAgent: [HistoryUserAgentModel().schema],
        weather: [HistoryWeatherModel().schema],
        geolocation: [HistoryGeolocationModel().schema],
        sessionID: String
    }
);

schema.query.byId = function(ids){
};

let nameModel = "History";

class HistoryModelClass
{
    static async bestViewed(categoryIds, itensId)
    {
        let queryArray = [
            {
                "$unwind": "$itens"
            },
            {
                "$group": {
                    _id: "$itens.id",
                    ocurrences: { "$sum": "$itens.views" },
                    categorys: { "$first": "$itens.product.categorys" }
                }
            },
            {
                "$match": {
                    _id: { $in: itensId || new Array }
                }
            }
        ]

        if(categoryIds != undefined && categoryIds != null)
        {
            queryArray.push({
                "$match": {
                    "categorys": {
                        "$in": categoryIds
                    }
                }
           })
        }

        let response = await this.aggregate(queryArray)
        let bestViewedCategorys = new Object()

        for(let match of response)
        {
            for(let catId of match.categorys)
            {
                if(bestViewedCategorys.hasOwnProperty(catId) == false)
                    bestViewedCategorys[catId] = 0
                
                bestViewedCategorys[catId] += match.ocurrences
            }
        }

        return bestViewedCategorys
    }
}

schema.loadClass(HistoryModelClass);

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}