import {Conector} from '../../database/mongodb/connector'
var mongoose = require('mongoose');

var schema = new mongoose.Schema(
    {
        date: {
            type: Date,
            default: new Date()
        },
        ocurrence: {
            type: Number,
            default: 1
        }
    }
);

let nameModel = "AccessModel";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}