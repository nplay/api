var mongoose = require('mongoose');
import {Conector} from "../../database/mongodb/connector"
import ItemModel from "../../storage/model/ItemModel"
import Origin from "./OriginModel"
import AccessModel from "./AccessModel";

let itemSchema = new mongoose.Schema(ItemModel().schema, { strict: false })

var schema = new mongoose.Schema(
    {
        id: {
            type: String,
            required: true
        },
        inserted_at: Date,
        product: itemSchema,
        views: {
            type: Number,
            default: 1
        },
        searchTypeDescription: {
            type: String,
            required: true
        },
        accessCollection: [AccessModel().schema],
        originCollection: [Origin().schema]
    }
);

schema.query.byId = function(ids){
};

let nameModel = "HistoryProspectItem";

class ModelClass
{
}

schema.loadClass(ModelClass);

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}