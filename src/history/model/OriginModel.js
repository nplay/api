
import {Conector} from '../../database/mongodb/connector'
var mongoose = require('mongoose');

var schema = new mongoose.Schema(
    {
        id: {
            type: String,
            required: true
        },
        title: {
            type: String,
            required: true
        },
        node: {
            id: {
                type: String,
                required: true
            },
            name: {
                type: String,
                required: true
            }
        }
    }
);

let nameModel = "HistoryProspectItemOrigin";

class ModelClass
{
}

schema.loadClass(ModelClass);

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}