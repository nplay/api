var mongoose = require('mongoose');

let Conector = require('../../database/mongodb/connector').Conector

var schema = new mongoose.Schema(
    {
        created_at: {
            type: Date,
            required: true,
            default: new Date()
        },
        value: {
            type: String,
            required: false,
            default: null
        },
        mobile: {
            type: Boolean,
            default: false
        }
    }
);

schema.query.byId = function(ids){
};

let nameModel = "HistoryUserAgent";

class HistoryModelClass
{
}

schema.loadClass(HistoryModelClass);

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}