var mongoose = require('mongoose');
import ItemModel from "../../storage/model/ItemModel"
import OriginModel from "./OriginModel";
import AccessModel from "./AccessModel";
let itemSchema = new mongoose.Schema(ItemModel().schema, { strict: false })

var schema = new mongoose.Schema(
    {
        id: {
            type: String,
            required: true
        },
        inserted_at: Date,
        product: itemSchema,
        views: {
            type: Number,
            default: 1
        },
        accessCollection: [AccessModel().schema],
        originCollection: [OriginModel().schema]
    }
);

schema.query.byId = function(ids){
};

let nameModel = "HistoryItem";

class ModelClass
{
}

schema.loadClass(ModelClass);

export default mongoose.model(nameModel, schema)