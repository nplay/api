
import {Conector} from '../../database/mongodb/connector'
var mongoose = require('mongoose');

var schema = new mongoose.Schema(
    {
        date: Date,
        city: String,
        region: String,
        countryCode: String,
        temp: Number,
        climate: String
    }
);

let nameModel = "HistoryWeather";

class ModelClass
{
}

schema.loadClass(ModelClass);

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}