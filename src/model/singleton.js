
class Singleton
{
	static get(instance)
	{
		Singleton.instances = Singleton.instances || new Object

		if(!Singleton.instances.hasOwnProperty(instance))
			Singleton.instances[instance] = Object.create(instance)
		
		return Singleton.instances[instance]
	}
}

export default Singleton