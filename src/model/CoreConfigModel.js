var mongoose = require('mongoose');
var schema = new mongoose.Schema(
    {
        path: {
            type: String,
            required: true
        },
        value: {
            type: String,
            required: true
        }
    }
);

class CoreConfigClass
{
    
}

let nameModel = "CoreConfig";
schema.loadClass(CoreConfigClass);

//mongoose.model("CoreConfig", schema)

module.exports = {
    SchemaCoreConfigModel: schema,
    Name: nameModel,
    CoreConfigModel: mongoose.model(nameModel, schema)
}