
import searcherRoutes from './routes/searches'

module.exports = (app) => {
   app.use('/ti', require('./tiController'));
   app.use('/storage', require('./storage/controller/IndexController'));

   app.use('/user', require('./user/controller/IndexController'));
   
   searcherRoutes(app);
   
   app.use('/category/', require('./category/controller/IndexController'));
   
   app.use('/themes/menu', require('./themes/menu/controller/IndexController'));
   app.use('/themes/site', require('./themes/site/controller/IndexController'));
   app.use('/themes/site/default', require('./themes/site/controller/DefaultController'));
   
   app.use('/themes/popup', require('./themes/popup/controller/IndexController'));
   
   app.use('/statistic/sale/summary', require('./statistic/sale/controller/SummaryController'));
   app.use('/statistic/sale/item/summary', require('./statistic/sale/item/controller/SummaryController'));
   app.use('/statistic/sale/item/best-seller', require('./statistic/sale/item/controller/BestSellerController'));
   
   app.use('/statistic/history/item/summary', require('./statistic/history/Item/controller/SummaryController'));
   app.use('/statistic/history/summary', require('./statistic/history/controller/SummaryController'));
   
   app.use('/statistic/request/summary', require('./statistic/request/controller/SummaryController'));
}