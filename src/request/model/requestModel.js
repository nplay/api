import DateHelper from "../../helper/DateHelper";

var mongoose = require('mongoose');

var schemaModel = new mongoose.Schema(
   {
       path: {
         type: String,
         required: true
       },
       method: {type: String, required: true},
       requests: {
           type: Number,
           default: 1
       },
       created_at: {
           type: Date,
           required: true,
           default: new Date(new DateHelper().toTimezone().toISOString())
       }
   }
);

class RequestClass
{
   
}

schemaModel.loadClass(RequestClass);
let nameModel = "Request";

export default
    (conn) => {
        conn = conn ? conn : Conector.connect()
        let hasModel = conn.models.hasOwnProperty(nameModel)
        return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
    }