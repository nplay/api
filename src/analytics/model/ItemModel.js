var mongoose = require('mongoose');
import Origin from "../../history/model/OriginModel"
import {Conector} from '../../database/mongodb/connector'

var schema = new mongoose.Schema(
    {
        id: {
            type: String,
            required: true
        },

        entityId: {
            type: String,
            required: true
        },

        name: {
            type: String,
            required: true
        },

        categorys: {
            type: Array,
            required: false
        },

        quantity: {
            type: Number,
            required: true,
        },

        price: {
            type: Number,
            required: true
        },

        originCollection: [Origin().schema]
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class ModelClass
{
}

schema.loadClass(ModelClass);
let nameModel = "AnalyticsItemModel";

module.exports = {
    AnalyticsItemModel: (conn) => {
        conn = conn ? conn : Conector.connect()
        let hasModel = conn.models.hasOwnProperty(nameModel)
        return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
    }
}