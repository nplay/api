var mongoose = require('mongoose');
import {AnalyticsItemModel} from "../model/ItemModel"

var schema = new mongoose.Schema(
    {
        identification: {
            type: String,
            index: true,
            unique: true
        },
        created_at: {
            type: Date,
            required: true
        },
        quantityItem: Number,
        subTotal: Number,
        total: Number,
        
        items: [AnalyticsItemModel().schema]
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class OrderModelClass
{
    static async bestSeller(categoryIds, itensId)
    {
        let queryArray = [
            {
                $unwind: "$items"
            },
            {
                $group: {
                    _id: "$items.id",
                    total: {
                        $sum: {
                            $multiply: ["$items.price", "$items.quantity"]
                        }
                    },
                    categorys: {
                        $first: "$items.categorys"
                    },
                    quantity: { $sum: "$items.quantity" }
                }
            },
            {
                $match: {
                    _id: { $in: itensId || new Array }
                }
            }
        ]

        if(categoryIds != undefined)
        {
            queryArray.push({
                "$match": {
                "categorys": {
                        "$in": categoryIds
                    }
                }
           })
        }

        return this.aggregate(queryArray)
    }
}

schema.loadClass(OrderModelClass);
let nameModel = "AnalyticsOrderModel";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}