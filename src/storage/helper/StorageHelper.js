
import ItemModel from "../../storage/model/ItemModel"

class StorageHelper
{
   find(query, projection)
   {
      return ItemModel().find(query, projection)
   }

   findOne(query, projection)
   {
      return ItemModel().findOne(query, projection)
   }
}

export default StorageHelper