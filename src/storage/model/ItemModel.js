var mongoose = require('mongoose');
import {Conector} from '../../database/mongodb/connector'

var schema = new mongoose.Schema(
    {
        created_at: Date,
        id: String,
        categorys: {
            type: Array
        },
        hasNew: {
            type: Boolean,
            default: false
        }
    }, { strict: false }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class ItemClass
{
    static async saveAll(itens)
    {
        let ids = itens.map((item) => { return item.id });

        let foundItens = await this.find().byId(ids);
        let idsFound = foundItens.map((item) => {return item.id});

        let newItens = itens.filter((item) => {
            let notExists = idsFound.indexOf(item.id) == -1;

            if(notExists)
                item.created_at = new Date;

            return idsFound.indexOf(item.id) == -1;
        });

        console.log('Novos itens:' + newItens.length)

        await this.insertMany(newItens)

        let modifiedItens = newItens.map(item => { return item.id });

        let updateItens = itens.filter(item => {
            return modifiedItens.indexOf(item.id) == -1;
        });

        for(let item of updateItens)
        {
            await this.updateOne({id: item.id}, {$set: item}, err => {
                if(null != err)
                {
                    console.log(err)
                    throw 'Falha na atualizacao do item: ' + item.id
                }
            })
        }

        console.log('Itens que foram atualizados: ' + updateItens.length);
    }
}

schema.loadClass(ItemClass);
let nameModel = "StorageItem";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}