
import {SchemaCoreConfigModel, Name} from "../../model/CoreConfigModel";

var router = express.Router();

router.route('/')
.get(function(req, res)
{
    let CoreConfigModel = mongoose.model(Name, SchemaCoreConfigModel);

    CoreConfigModel.find({path: "url"})
    .then((result) => {
        ResponseHelper.setSuccess(res, 'OK');
    })
    .catch((error) => {
        ResponseHelper.setError(res, "Falha ao processar");
    });
})
.post(function(req, res)
{
    let body = req.body;
    let CoreConfigModel = mongoose.model(Name, SchemaCoreConfigModel);

    //Search coreConfig exists
    CoreConfigModel.findOne({path: "url"})
    .then((result) =>
    {
       if(result == null)
       {
            let objCoreConfig = new CoreConfigModel({path: "url", "value": req.body.url });

            objCoreConfig.save().then(() =>
            {
                ResponseHelper.setSuccess(res, objCoreConfig);
            }, error => 
            {
                ResponseHelper.setError(res, error.message);
            });
       }
       else
          ResponseHelper.setWarning(res, "Objeto já existe, tente o PUT");
    });
})
.put(function(req, res)
{
    let body = req.body;
    let CoreConfigModel = mongoose.model(Name, SchemaCoreConfigModel);

    CoreConfigModel.findOne({path: "url"})
    .then((doc) =>
    {
        if(doc == null)
            return ResponseHelper.setWarning(res, "Objeto não existe");

        CoreConfigModel.updateOne({_id: doc._id}, {value: body.url}).then((e) => {
            ResponseHelper.setSuccess(res, "Atualizado");
        },
        error => {
            ResponseHelper.setError(res, );
        });
    },
    error => {
        ResponseHelper.setError(res, error.message);
    });
});

module.exports = router;