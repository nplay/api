
import RequestModel from '../../../request/model/requestModel'
import AbstractSummaryHelper from "../../helper/AbstractSummaryHelper"

class SummaryHelper extends AbstractSummaryHelper
{
    constructor(startDate, endDate, cycles, additionalData)
    {
        super(startDate, endDate, cycles, additionalData)
    }

    async _byInterval(startDate, endDate)
    {
        return await RequestModel().find({
            created_at: {
               $gte: new Date(startDate),
               $lte: new Date(endDate)
            }
         }, {
            _id: 0,
            __v: 0
         })
         .sort({requests: this._additionalData.sort})
         .limit(parseInt(this._additionalData.limit))
    }
}

export default SummaryHelper