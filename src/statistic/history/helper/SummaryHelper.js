
import HistoryModel from "../../../history/model/HistoryModel"
import AbstractSummaryHelper from "../../helper/AbstractSummaryHelper"

class SummaryHelper extends AbstractSummaryHelper
{
    constructor(startDate, endDate, cycles)
    {
        super(startDate, endDate, cycles)
    }

    async _byInterval(startDate, endDate)
    {
        return {
            qty: await HistoryModel().find({
                created_at: {
                    $gte: new Date(startDate),
                    $lte: new Date(endDate)
                }
            }).count()
        }
    }
}

export default SummaryHelper