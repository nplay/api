var router = express.Router();

import SummaryHelper from "../helper/SummaryHelper"

router.route('/')
.post(function(req, res)
{
    let fnProcess = async () => {
        let startDat = req.body.startDate
        let endDat = req.body.endDate
        let cycles = req.body.cycles

        let summaryHlp = new SummaryHelper(startDat, endDat, cycles)
        return await summaryHlp.get()
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    })
});

module.exports = router;