
import HistoryModel from "../../../../history/model/HistoryModel"
import AbstractSummaryHelper from "../../../helper/AbstractSummaryHelper"

class SummaryHelper extends AbstractSummaryHelper
{
    constructor(startDate, endDate, cycles, key)
    {
        super(startDate, endDate, cycles)
        this.itemKey = this._setItemKey(key);
    }

    _setItemKey(key)
    {
        let availableKeys = new Object({
            item: 'itens',
            prospect: 'prospectItens'
        })

        if(!availableKeys.hasOwnProperty(key))
            throw new Error(`Key: ${key} not available`)
        
        return availableKeys[key];
    }

    async _byInterval(startDate, endDate)
    {
        return await HistoryModel().aggregate([
            {
                $unwind: `$${this.itemKey}`
            },
            {
                $match: {
                    [`${this.itemKey}.inserted_at`]: {
                        $gte: new Date(startDate),
                        $lte: new Date(endDate)
                    }
                }
            },
            {
                $group: {
                    _id: [`${this.itemKey}.id`],
                    qty: { $sum: 1 }
                }
            }
        ])
    }
}

export default SummaryHelper