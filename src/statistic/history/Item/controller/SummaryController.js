    var router = express.Router();

import SummaryHelper from "../helper/SummaryHelper"

router.route('/:key')
.post(function(req, res)
{
    let fnProcess = async () => {
        let startDat = req.body.startDate
        let endDat = req.body.endDate
        let cycles = req.body.cycles
        let key = req.params.key

        let summaryHlp = new SummaryHelper(startDat, endDat, cycles, key)
        return await summaryHlp.get()
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    }, error => {
        ResponseHelper.setError(res, error.toString())
    })
});

module.exports = router;