
import DateHelper from "../helper/DateHelper"

class OrderModel
{
    constructor()
    {
        this.startAt = new Date().toISOString(),
        this.endAt = new Date().toISOString(),
        this.collection = new Array()
    }
}

class AbstractSummaryHelper
{
    constructor(startDate, endDate, cycles, additionalData)
    {
        this._additionalData = additionalData || new Object
        this._dateHlp = new DateHelper(startDate, endDate, cycles)
    }

    async get()
    {
        let dateRanges = this._dateHlp.getDateCycles();
        let orderCollection = new Array();

        for(let range of dateRanges)
        {
            let results = await this._byInterval(range.startDate, range.endDate)
            
            let orderModel = new OrderModel()
            orderModel.startAt = range.startDate
            orderModel.endAt = range.endAt
            orderModel.collection = results

            orderCollection.push(orderModel)
        }

        return orderCollection
    }

    async _byInterval(startDate, endDate)
    {
        throw new Error('Please implement in child class...')
    }    
}

export default AbstractSummaryHelper