
import DateHelperAbstract from "../../helper/DateHelper"

class DateHelper extends DateHelperAbstract
{
    constructor(startDate, endDate, cycles)
    {
        super();

        this.startDate = startDate
        this.endDate = endDate

        this.cycles = cycles
        this.diffDays = 0
        this.dateCycles = new Array        
    }

    getDateCycles()
    {
        this._prepare()
        
        return this.dateCycles
    }

    _prepare()
    {
        this.diffDays = this.diff(this.startDate, this.endDate)
        this._setDateCycles();
    }

    _setDateCycles(endDate, curCycle)
    {
        if(endDate == undefined)
        {
            this.dateCycles.push({
                startDate: this.startDate,
                endDate: this.endDate
            })

            endDate = this.startDate
            curCycle = 1

            if(this.diffDays == 0)
                return true;
        }

        if(curCycle > this.cycles)
            return true;

        let startDate = this.subtract(this.diffDays, endDate)

        this.dateCycles.push({
            startDate: startDate,
            endDate: endDate
        })
 
        this._setDateCycles(startDate, ++curCycle)
        
        return true;
    }
}

export default DateHelper