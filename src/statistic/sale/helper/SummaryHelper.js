
import AnalyticsOrderModel from "../../../analytics/model/OrderModel"
import AbstractSummaryHelper from "../../helper/AbstractSummaryHelper"

class SummaryHelper extends AbstractSummaryHelper
{
    constructor(startDate, endDate, cycles)
    {
        super(startDate, endDate, cycles)
    }

    async _byInterval(startDate, endDate)
    {
        return await AnalyticsOrderModel().aggregate([
            {
                $unwind: "$items"
            },
            {
                $match: {
                    created_at: {
                        $gte: new Date(startDate),
                        $lte: new Date(endDate)
                    }
                }
            },
            {
                $group: {
                    _id: "$identification",
                    totalPrice: { $sum: "$items.price" },
                    itensQty: { $sum: "$items.quantity" },
                    created_at: { $first: "$created_at"}
                }
            }
        ]);
    }
}

export default SummaryHelper