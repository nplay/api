var router = express.Router();

//import BestSellerHelper from "../helper/BestSellerHelper"

router.route('/')
.post(function(req, res)
{
    let fnProcess = async () => {
        let startDat = req.body.startDate
        let endDat = req.body.endDate
        let cycles = req.body.cycles

        let additionalData = {
            limit: req.body.limit,
            sort: {desc: -1, asc: 1}[req.body.sort]
        }

        return new Array

        //let summaryHlp = new BestSellerHelper(startDat, endDat, cycles, additionalData)
        //return await summaryHlp.get()
    }

    fnProcess().then(response => {
        return ResponseHelper.setSuccess(res, response)
    })
});

module.exports = router;