
import AnalyticsOrderModel from "../../../../analytics/model/OrderModel"
import AbstractSummaryHelper from "../../../helper/AbstractSummaryHelper"

class SummaryHelper extends AbstractSummaryHelper
{
    constructor(startDate, endDate, cycles)
    {
        super(startDate, endDate, cycles)
    }

    async _byInterval(startDate, endDate)
    {
        return await AnalyticsOrderModel().aggregate([
            {
                $match: {
                    "created_at": {
                        $gte: new Date(startDate),
                        $lte: new Date(endDate)
                    }
                }
            },
            {
                $unwind: "$items"
            },
            {
                $group: {
                    _id: "$items.searchTypeDescription",
                    items: {
                        $addToSet: "$items"
                    },
                    total: {
                        $sum: {
                            $multiply: [ "$items.price", "$items.quantity" ]
                        }
                    }
                }
            },
            {
                $project: {
                    "qty": { $size: "$items" },
                    "total": "$total"
                }
            }
        ]);
    }
}

export default SummaryHelper