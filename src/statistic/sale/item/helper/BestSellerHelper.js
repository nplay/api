
import AnalyticsOrderModel from "../../../../analytics/model/OrderModel"
import AbstractSummaryHelper from "../../../helper/AbstractSummaryHelper"

class BestSellerHelper extends AbstractSummaryHelper
{
    constructor(startDate, endDate, cycles, additionalData)
    {
        super(startDate, endDate, cycles, additionalData)
    }

    _prepareQuery(startDate, endDate)
    {
        return [
            {
                $match: {
                    "created_at": {
                        $gte: new Date(startDate),
                        $lte: new Date(endDate)
                    }
                }
            },
            {
                $unwind: "$items"
            },
            {
                $group: {
                    _id: "$items.id",
                    qty: { $sum: "$items.quantity" }
                }
            },
            {
                $project: {
                    "total": "$total",
                    "qty": "$qty"
                }
            },
            {
                $sort: {
                    "qty": this._additionalData.sort
                }
            },
            { $limit : this._additionalData.limit }
        ]
    }

    async _byInterval(startDate, endDate)
    {
        let query = this._prepareQuery(startDate, endDate);
        return await AnalyticsOrderModel().aggregate(query);
    }
}

export default BestSellerHelper