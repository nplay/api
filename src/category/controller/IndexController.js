import CategoryTreeSortHelper from '../helper/CategoryTree/SortHelper'
import SortLevelError from "../helper/CategoryTree/Sort/Error/LevelError";

var router = express.Router();

router.route('/')
.get(function(req, res)
{
})

router.route('/sort').post(function(req, res)
{
    let fnProcess = async () => {
        let sortCollection = req.body.sorts
        let menu = req.body.menu || null
        let categorySortHelper = new CategoryTreeSortHelper(menu)

        for(let sort of sortCollection)
        {
            try
            {
                await categorySortHelper.sortLevel(sort.by, sort.level)
            }
            catch (error)
            {
                if(!(error instanceof SortLevelError))
                    throw error
            }
        }
        
        return await categorySortHelper.getTree()
    }

    fnProcess().then(response => {
        ResponseHelper.setSuccess(res, response)
    })
});
module.exports = router;