
import {MountHelper} from "../helper/CategoryTree/Mount"

class CategoryTreeHelperClass
{
    constructor(tree)
    {
        this._tree = tree == undefined ? null : tree;
        this._mountHelper = new MountHelper();
    }

    async _mount()
    {
       this._tree = await this._mountHelper.tree()
       return this
    }

    getLevel(nodes)
    {
        if(nodes == undefined)
        {
            nodes = new Array()

            let iterator = this.iterate(null, false)
            let cursor = iterator.next()

            while(cursor.done == false)
            {
                nodes.push(cursor.value)
                cursor = iterator.next()
            }
        }

        let currentNodes = new Array()
        let nextNodes = new Array()

        for(let node of nodes)
        {
            currentNodes.push(node)
            
            for(let children of node.children)
                nextNodes.push(children)
        }

        return {
            currentLevel: currentNodes,
            nextLevel: nextNodes
        }
    }

    *iterate(currentNode, iterateChild)
    {
        if(currentNode == undefined)
        {
            for(let root of this._tree)
                yield* this.iterate(root, iterateChild)
        }
        else
        {
            yield currentNode
            
            if(iterateChild == false)
                return;
                
            for(let child of currentNode.children)
                yield*  this.iterate(child, iterateChild)
        }
    }

    /**
     * Returns node relative into path passed
     * @param {String} path
     */
    *iteratePath(path)
    {
        let node = this.search(path)

        if(typeof node == undefined)
            throw new Error(`Fail iteratePath ${path}, node not found`)

        //Transpass node
        yield node

        let partsPath = path.split(' > ')
        partsPath.pop()
        path = partsPath.join()

        if(Array.isArray(path) == false) //Final path
        {
            node = this.search(path)
            return node == false ? false : yield node
        }
        else
            yield* this.iteratePath(path)
    }

    searchByAttribute(attribute, value)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();

        while(cursor.done == false)
        {
            let node = cursor.value

            if(node.hasOwnProperty(attribute) && node[attribute] == value)
                return node

            cursor = iterator.next();
        }

        return false;
    }

    search(path)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();

        while(cursor.done == false)
        {
            let node = cursor.value

            if(node.path == path)
                return node

            cursor = iterator.next();
        }

        return false;
    }

    assignAttributes(attributes)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();

        while(cursor.done == false)
        {
            let node = cursor.value

            if(node != undefined)
                Object.assign(node, attributes)

            cursor = iterator.next();
        }
    }

    sortByAttribute(attributeName, node)
    {
        if(node)
            return this.sort(node.children, attributeName)

        let iterator = this.iterate();
        let cursor = iterator.next();

        while(cursor.done == false)
        {
            let node = cursor.value

            this.sort(node.children, attributeName)
            cursor = iterator.next();
        }
    }

    sort(node, attributeName, customFilter)
    {
        node.sort((a, b) => {
            if(!a.hasOwnProperty(attributeName) || !b.hasOwnProperty(attributeName))
                throw new Error(`Property: ${attributeName} not exists in object sort`)

            let attrA = a[attributeName]
            let attrB = b[attributeName]

            if(customFilter)
                return customFilter(attrA, attrB)

            let result = attrA - attrB

            if(result < 0)
                return 1;
            else if(result > 0)
                return -1;
            else
                return 0;
        })
    }
}

module.exports = {
    CategoryTreeHelper: CategoryTreeHelperClass
}