
import CategoryModel from "../../model/CategoryModel"

class MountClass
{
    async tree()
    {
        let categories = await CategoryModel().find({}, {_id: false, __v: false})
        return await CategoryModel().getTree(categories)
    }
}

module.exports = {
    MountHelper: MountClass
}