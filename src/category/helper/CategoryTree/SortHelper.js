
import {CategoryTreeHelper} from '../CategoryTreeHelper'
import { BestSellerHelper } from "../../../searches/category/bestseller/Helper"
import { BestViewedHelper } from "../../../searches/category/bestviewed/Helper"
import LevelError from './Sort/Error/LevelError';

class CategoryTreeSortHelper
{
   constructor(tree)
   {
      this._categoryTreeHelper = new CategoryTreeHelper(tree)

      this._sortTypes = new Object({
         [CategoryTreeSortHelper.SORT_ALPHABETICAL]: CategoryTreeSortHelper.SORT_ALPHABETICAL,
         [CategoryTreeSortHelper.SORT_BEST_SELLER]: CategoryTreeSortHelper.SORT_BEST_SELLER,
         [CategoryTreeSortHelper.SORT_BEST_VIEWED]: CategoryTreeSortHelper.SORT_BEST_VIEWED
      })
   }

   static get SORT_ALPHABETICAL()
   {
      return 'sortAlphabetical'
   }

   static get SORT_BEST_SELLER()
   {
      return 'sortBestSeller'
   }

   static get SORT_BEST_VIEWED()
   {
      return 'sortBestViewed'
   }

   async mount()
   {
      if(this._categoryTreeHelper._tree == null)
         return await this._categoryTreeHelper._mount()

      return true
   }

   async getTree()
   {
      await this.mount()
      return this._categoryTreeHelper._tree
   }

   async sortLevel(typeSort, level)
   {
      let parentLevel = level - (level == 1 ? 0 : 1)
      let parentTrees = await this._getLevel(parentLevel)
      let sortMethod = this._getSortMethod(typeSort)

      if(level > 1)
      {
         for(let parentTree of parentTrees)
         {
            let childTree = parentTree.children
            await this[sortMethod](childTree)
         }
      }
      else
      {
         await this[sortMethod](parentTrees)
         this._categoryTreeHelper._tree = parentTrees
      }
   }

   async _getLevel(level)
   {
      await this.mount()

      let levels = this._categoryTreeHelper.getLevel()

      for(let i = 1; i < level; ++i)
         levels = this._categoryTreeHelper.getLevel(levels.nextLevel)

      if(levels.currentLevel.length == 0)
         throw new LevelError(`Level ${level} is empty`)
      
      return levels.currentLevel
   }

   _getSortMethod(type)
   {
      if(this._sortTypes.hasOwnProperty(type) == false)
         throw new Error(`Sort type ${type} not exists...`)

      return this._sortTypes[type]
   }

   async sortBestSeller(childrens)
   {
      let bestCategorys = await new BestSellerHelper().bestSellerCategorys()
      this._categoryTreeHelper.assignAttributes({ sells: 0 })

      for (let key of Object.keys(bestCategorys))
      {
         let categoryID = key
         let sells = bestCategorys[key]

         let node = this._categoryTreeHelper.searchByAttribute('id', categoryID)
         if(node == null)
            continue;
         
         node.sells += sells
      }

      this._categoryTreeHelper.sortByAttribute('sells', {children: childrens})
   }

   async sortBestViewed(childrens)
   {
      let bestViewed = await new BestViewedHelper().bestViewed()
      this._categoryTreeHelper.assignAttributes({ views: 0 })

      for (let key of Object.keys(bestViewed))
      {
         let categoryID = key
         let views = bestViewed[key]

         let node = this._categoryTreeHelper.searchByAttribute('id', categoryID)
         if(node == null)
            continue;
         
         node.views += views
      }

      this._categoryTreeHelper.sortByAttribute('views', {children: childrens})
   }

   async sortAlphabetical(node)
   {
      let method = (a, b) => {
         let compareResult =  a.localeCompare(b)
         return compareResult
      }

      this._categoryTreeHelper.sort(node, 'name', method)
   }
}

export default CategoryTreeSortHelper