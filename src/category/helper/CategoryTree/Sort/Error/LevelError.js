
class LevelError
{
   constructor(message) {
      this.message = message || 'Level fail';
   }
}

 export default LevelError