var mongoose = require('mongoose');
var schema = new mongoose.Schema(
    {
        name: String,
        
        id:
        {
            type: Number,
            required: true,
            unique: true
        },

        path: {
            type: String,
            required: false,
            default: null
        },

        parent_id:
        {
            type: Number,
            required: true
        },

        position: {
            type: String,
            required: true
        }
    }
);

schema.query.byId = function(ids){
    return this.find({id: {$in: ids}});
};

class CategoryClass
{
    static getTree(categorieCollection)
    {
        let arrayCat = categorieCollection.map(cat => {
            return cat.toObject();
        })

        let fnListToTree = (list) => {
            var map = {}, node, roots = [], i;
    
            for (i = 0; i < list.length; i += 1)
            {
                map[list[i].id] = i; // initialize the map
                list[i].children = []; // initialize the children
            }

            for (i = 0; i < list.length; i += 1)
            {
                node = list[i];

                let parentNodeIdx = map[node.parent_id]
                
                if(parentNodeIdx === undefined)
                    roots.push(node)
                else
                    list[parentNodeIdx].children.push(node);
            }
    
            return roots;
        }
        
        return fnListToTree(arrayCat)
    }
}

schema.loadClass(CategoryClass);
let nameModel = "Category";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}