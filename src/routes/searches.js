
//import PlanRequestMiddleware from '../middleware/planRequestMiddleware'
//import RequestMiddleware from '../middleware/requestMiddleware'
import PlanResourceMiddleware from '../middleware/planResourceMiddleware'

import SearchesItemIndexController from '../searches/item/searches/IndexController'
import HistoryItemRouter from '../searches/history/item/controller/IndexController'
import SearchesCategoryRouter from '../searches/category/IndexController'
import SearchesItemBestSeller from '../searches/item/searches/bestSeller/controller/BestSellerController'
import SearchesItemBestViewed from '../searches/item/searches/bestViewed/controller/BestViewedController'
import SearchesItemNew from '../searches/item/searches/new/controller/NewController'
import SearchesItemPromotional from '../searches/item/searches/promotional/controller/PromotionalController'
import SearchesItemViewToView from '../searches/item/ViewToViewController'
import SearchesItemSimilar from '../searches/item/SimilarController'
import SearchesItemBuyToBuy from '../searches/item/BuyToBuyController'

module.exports = (app) => {
    
    let getRouter = () => {
        let router = express.Router();
        router.use(async (req, res, next) => {

            let resourceMidleware = new PlanResourceMiddleware(res, req, {
                resource: PlanResourceMiddleware.RESOURCE_VITRINE
            })

            let middlewares = [resourceMidleware]

            for(let middleware of middlewares)
            {
               if(!await middleware.run())
                return await middleware.resFail()
            }

            return next()
        })

        return router
    }

    app.use('/searches/history/item', HistoryItemRouter( getRouter() ))

    app.use('/searches/category', SearchesCategoryRouter( getRouter() ));
    
    app.use('/searches/item/best-seller', SearchesItemBestSeller( getRouter() ));

    app.use('/searches/item/best-viewed', SearchesItemBestViewed( getRouter() ));

    app.use('/searches/item/new', SearchesItemNew( getRouter() ));

    app.use('/searches/item/promotional', SearchesItemPromotional( getRouter() ));

    app.use('/searches/item/view-to-view', SearchesItemViewToView( getRouter() ));
    
    app.use('/searches/item/similar', SearchesItemSimilar( getRouter() ));

    app.use('/searches/item/buy-to-buy', SearchesItemBuyToBuy( getRouter() ));

    app.use('/searches/item/',  SearchesItemIndexController( getRouter() ));
}