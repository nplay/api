import {Config as configFile} from "core"

var httpContext = require('express-http-context');
const { Seeder } = require('mongo-seeding');

import DBConfig from "./conector/model/dbConfig";
import ConnectionManagerModel from "./conector/model/connectionManagerModel";

class Conector
{
    constructor()
    {
        this._dbConfig = new DBConfig(configFile.database)
    }

    get connManagerModel()
    {
        if(!Conector._connManagerModel)
            Conector._connManagerModel = new ConnectionManagerModel(this._dbConfig)

        return Conector._connManagerModel
    }

    async runSeed(path, customDB)
    {
        let conStr = this.connManagerModel.getConnectionStr(customDB)

        let config = {
            database: conStr,
            dropDatabase: false,
            dropCollections: true
        };

        try {
            let conn = this.connect(customDB)

            let seeder = new Seeder(config);
            let collections = seeder.readCollectionsFromPath(path);
    
            for(let c of collections)
            {
                let dbCollection = conn.collection(c.name)

                await dbCollection.insert(c.documents)
            }
        } catch (error) {
            console.log(`ERROR SEED ${path} - ${customDB}`)
        }
    }
    
    connect(db)
    {
        let finalDb = db || this.getDB() || 'preload'
        let conn = this.connManagerModel.retrive(finalDb)
        
        return conn
    }

    getDB()
    {
        let store = new Object(httpContext.get('store'))
        return store.hasOwnProperty('database') ? store.database : null
    }

    getToken()
    {
        return httpContext.get('token')
    }

    connectCore()
    {
        return this.connManagerModel.retrive(this._dbConfig.db)
    }
}

module.exports = {
    Conector: new Conector()
}