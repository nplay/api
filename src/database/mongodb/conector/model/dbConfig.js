
class DBConfig
{
    constructor(configFile)
    {
        this.host = configFile.host;
        this.port = configFile.port;
        this.user = configFile.user;
        this.pwd = configFile.pwd;
        this.db = configFile.db
    }
}

export default DBConfig