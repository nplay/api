
var mongoose = require('mongoose');

mongoose.connection.on('error', err => {
    console.log(err)
});

class ConnectionManagerModel
{
    constructor(dbConfig)
    {
        this._dbConfig = dbConfig
    }

    get conexoes()
    {
        if(!ConnectionManagerModel._connections)
            ConnectionManagerModel._connections = new Object

        return ConnectionManagerModel._connections
    }

    retrive(db)
    {
        if(this.hasConnection(db) == false)
            this.create(db)
        
        return this.conexoes[db]
    }

    hasConnection(db)
    {
        return this.conexoes.hasOwnProperty(db)
    }

    create(db)
    {
        let connString = this.getConnectionStr(db)

        this.conexoes[db] = mongoose.createConnection(connString, {
            useNewUrlParser: true,
            poolSize: 1
        })

        return this.conexoes[db]
    }

    getConnectionStr(db)
    {
        return `mongodb://${this._dbConfig.host}:${this._dbConfig.port}/${db}`
    }
}

export default ConnectionManagerModel