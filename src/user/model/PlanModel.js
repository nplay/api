var mongoose = require('mongoose');

import {Conector} from '../../database/mongodb/connector'

import PlanEnum from './PlanEnum'

var schema = new mongoose.Schema(
    {
        id: {
            type: Number,
            default: PlanEnum[0].ID
        },

        name: {
            type: String,
            default: PlanEnum[0].name
        },

        requestsLimit: {
            type: Number,
            default: PlanEnum[0].requestsLimit
        },

        resources: {
            popup: {
                type: Boolean,
                default: PlanEnum[0].resources.popup
            },
            vitrine: {
                type: Boolean,
                default: PlanEnum[0].resources.vitrine
            },
            category: {
                type: Boolean,
                default: PlanEnum[0].resources.category
            },
            customLayout: {
                type: Number,
                default: PlanEnum[0].resources.customLayout
            },
            supportLevel: {
                type: String,
                default: PlanEnum[0].resources.supportLevel
            }
        }
    }
);

class Class
{
}

schema.loadClass(Class);
let nameModel = "Plan";

export default  (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}