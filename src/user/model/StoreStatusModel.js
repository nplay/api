var mongoose = require('mongoose');

import {Conector} from '../../database/mongodb/connector'

var schema = new mongoose.Schema(
    {
        code: {
            type: String,
            required: true
        },

        status: {
            type: String,
            required: true
        },

        updated_at: {
            type: Date
        },

        technicalData: {
            type: String,
            required: false
        }
    }
);

class Class
{
}

schema.loadClass(Class);
let nameModel = "StoreStatusModel";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}