
export default {
    0: {
        name: "Avaliação",
        id: 0,
        requestsLimit: 100000,
        resources: {
            popup: false,
            vitrine: true,
            category: true,
            customLayout: 2,
            supportLevel: "basic"
        }
    },
    1: {
        name: "Profissional",
        id: 1,
        requestsLimit: 300000,
        resources: {
            popup: true,
            vitrine: true,
            category: true,
            customLayout: 3,
            supportLevel: "priority"
        }
    },
    2: {
        name: "Personalizado",
        id: 2,
        requestsLimit: 1000000,
        resources: {
            popup: true,
            vitrine: true,
            category: true,
            customLayout: -1, //unlimited
            supportLevel: "premium"
        }
    }
}