var mongoose = require('mongoose');

import Store from './StoreModel'

var schemaUserModel = new mongoose.Schema(
    {
        email: {
            type: String,
            required: true
        },

        fullName: {
            type: String,
            required: true
        },

        phone: {
            type: String,
            required: true
        },
        
        password: {type: String, required: true},

        privateKey: {
            type: String,
            required: true
        },

        stores: [Store().schema]
    }
);

class UserClass
{
    static findActives()
    {
      return this.find({});
    }

    static byPublicKeys(publicKeys)
    {
        return this.findOne({
            "stores.applications.keys.value": { $in: publicKeys }
        })
    }

    static storeMath(math)
    {
        return this.aggregate(
            [
                {
                    $unwind :  {
                        path: "$stores", includeArrayIndex: "idxStore"
                    }
                },
                
                math,
                
                {
                    $project: {
                        "store": "$stores",
                        "idxStore": "$idxStore"
                    }
                },
                
                {
                    $limit: 1
                }
            ]
        );
    }

    static storeByDB(database)
    {
        return this.storeMath({
            $match: {"stores.database": database}
        })
    }

    static storeByPublicKeys(publicKeys)
    {
        return this.storeMath(                {
            $match: {
                "stores.applications.keys.value": { $in: publicKeys }
            }
        })
    }
}

schemaUserModel.loadClass(UserClass);
let nameModel = "Users";

module.exports = {
    SchemaUserModel: schemaUserModel,
    UserModel: (conn) => {
        conn = conn ? conn : Conector.connect()
        let hasModel = conn.models.hasOwnProperty(nameModel)
        return hasModel ? conn.model(nameModel) : conn.model(nameModel, schemaUserModel)
    }
}