var mongoose = require('mongoose');

import {Conector} from '../../database/mongodb/connector'

import KeyModel from './KeyModel'

var schema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        },

        keys: [KeyModel().schema]
    }
)

class Class
{
}

schema.loadClass(Class);
let nameModel = "Application";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}