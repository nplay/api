var mongoose = require('mongoose');

import {Conector} from '../../database/mongodb/connector'

var schema = new mongoose.Schema(
    {
        alias: {
            type: String,
            required: true,
            default: "default"
        },

        value:  {
            type:String,
            required: true
        }
    }
)

class Class
{
}

schema.loadClass(Class);
let nameModel = "Key";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}