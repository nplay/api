var mongoose = require('mongoose');

import {Conector} from '../../database/mongodb/connector'
import PlanModel from './PlanModel'
import ApplicationModel from './ApplicationModel'
import StoreStatusModel from './StoreStatusModel';

var schema = new mongoose.Schema(
    {
        plan: {
            type: PlanModel().schema,
            required: true,
            default: new PlanModel()
        },

        applications: [ApplicationModel().schema],

        platform: {
            type: String,
            required: true
        },

        database: {
            type: String,
            required: true,
            unique: true
        },

        additionalData: new mongoose.Schema({}, { strict: false }),

        instalationStep: {
            type: Number,
            required: true,
            default: 1
        },

        status: [StoreStatusModel().schema],

        url: {type: String, required: false}
    }
);

class Class
{
}

schema.loadClass(Class);
let nameModel = "Store";

export default (conn) => {
    conn = conn ? conn : Conector.connect()
    let hasModel = conn.models.hasOwnProperty(nameModel)
    return hasModel ? conn.model(nameModel) : conn.model(nameModel, schema)
}