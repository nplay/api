import { UserModel } from "../model/UserModel";
import { Conector } from "../../database/mongodb/connector";
import UserHelper from "../../helper/UserHelper";

var bcrypt = require("bcrypt");

var router = express.Router();

router.route("/login").post(function (req, res) {
  let fnProcess = async () => {
    let responseData = {
      success: false,
      message: null,
    };

    let conn = Conector.connectCore();
    let body = req.body;

    let user = await UserModel(conn).findOne({ email: body.email });

    if (null == user) {
      responseData.message = "Usuário não encontrado";
      return responseData;
    }

    let passwordTrue = bcrypt.compareSync(body.password, user.password);
    responseData.success = passwordTrue;

    if (!passwordTrue) {
      responseData.message = "Dados invalidos";
      return responseData;
    }

    responseData.message = user.toObject();
    delete responseData.message.password;

    return responseData;
  };

  fnProcess().then(
    (response) => {
      if (response.success == false)
        ResponseHelper.setWarning(res, response.message);
      else ResponseHelper.setSuccess(res, response);
    },
    (error) => {
      ResponseHelper.setError(res, error.toString());
    }
  );
});

router.route("/me").get(function (req, res) {
  let fnProcess = async () => {
    let responseData = {
      success: false,
      message: null,
    };

    let conn = Conector.connectCore();
    let token = req.header("Authorization-token");

    let user = null;
    if (token) {
      user = await UserModel(conn).byPublicKeys(token);
    } else {
      responseData.message = "Usuário não está logado.";
    }

    if (user) {
      responseData.message = user.toObject();
      delete responseData.message.password;
      responseData.success = true;
    } else {
      responseData.message = "Usuário não existe.";
    }

    return responseData;
  };

  fnProcess().then(
    (response) => {
      if (response.success == false)
        ResponseHelper.setWarning(res, response.message);
      else ResponseHelper.setSuccess(res, response);
    },
    (error) => {
      ResponseHelper.setError(res, error.toString());
    }
  );
});

router.patch("/store", function (req, res) {
  let methods = {
    connCore: null,

    async initConn() {
      this.connCore = await Conector.connectCore();
    },

    async updateStore(data) {
      let entity = await new UserHelper().storeByPublicKeys([
        Conector.getToken(),
      ]);

      if (!entity) return null;

      let user = await new UserHelper().getUser();
      user.stores[entity.idxStore] = Object.assign(
        user.stores[entity.idxStore],
        data
      );

      await user.save();
      return user;
    },
  };

  let fnProcess = async () => {
    try {
      await methods.initConn();
      let response = await methods.updateStore(req.body);

      if (response == null)
        return ResponseHelper.setWarning(res, "Usuario não encontrado");

      ResponseHelper.setSuccess(res, response);
    } catch (error) {
      ResponseHelper.setError(res, error.toString());
    }
  };

  fnProcess();
});

module.exports = router;
